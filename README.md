# Hcare - backend
[![pipeline status](https://gitlab.com/miechayakorn/BBQ-backend/badges/1.0/pipeline.svg)](https://gitlab.com/miechayakorn/BBQ-backend/-/commits/1.0)

## Project setup
```
npm install
```
### ENV : change .env.example to .env and Modify variable
```
mv .env.example .env
vi .env
```
### Compiles for development
```
adonis serve --dev
```

## Build mysql, phpmyadmin by Docker-compose
```
vi docker-compose.yml
docker-compose up -d --build mysql phpmyadmin
```
## Build Hcare Project by Docker
```
cd hcare_backend
docker build --pull --no-cache -t hcare_backend .
docker run -d -p 3333:3333 --name hcare_adonis --env-file .env --restart=always hcare_backend
```
