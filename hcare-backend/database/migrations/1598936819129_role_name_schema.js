"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class RoleNameSchema extends Schema {
  up() {
    this.create("role_names", (table) => {
      table.increments("rn_id");
      table.string("role_name").notNullable();
      table.boolean("is_active").default(true);
      table.integer("add_id");
      table.integer("edit_id");
      table.timestamps();
    });
  }

  down() {
    this.drop("role_names");
  }
}

module.exports = RoleNameSchema;
