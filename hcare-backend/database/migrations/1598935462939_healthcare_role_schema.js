"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class HealthcareRoleSchema extends Schema {
  up() {
    this.create("healthcare_roles", (table) => {
      table.increments("role_id").primary();
      table.integer("rn_id").notNullable; // FK
      table.integer("account_id").notNullable;
      table.boolean("is_active");
      table.integer("add_id");
      table.integer("edit_id");
      table.timestamps();
    });
  }

  down() {
    this.drop("healthcare_roles");
  }
}

module.exports = HealthcareRoleSchema;
