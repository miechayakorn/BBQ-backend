"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class HistorySchema extends Schema {
  up() {
    this.create("histories", (table) => {
      table.increments("h_id").primary();
      table.string("type_name").notNullable();
      table.string("booking_id").notNullable();
      table.date("date").notNullable();
      table.time("time_in").notNullable();
      table.time("time_out").notNullable();
      table.text("comment_from_user");
      table.text("comment_from_staff");
      table.string("account_id");
      table.string("prefix_doctor");
      table.string("doctor_name");
      table.timestamps();
    });
  }

  down() {
    this.drop("histories");
  }
}

module.exports = HistorySchema;
