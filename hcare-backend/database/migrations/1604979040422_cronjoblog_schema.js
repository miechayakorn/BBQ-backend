"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CronjoblogSchema extends Schema {
  up() {
    this.create("cronjoblogs", (table) => {
      table.increments("c_id").primary();
      table.timestamps();
    });
  }

  down() {
    this.drop("cronjoblogs");
  }
}

module.exports = CronjoblogSchema;
