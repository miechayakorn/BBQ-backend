"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class RoleName extends Model {
  static get primaryKey() {
    return "rn_id";
  }
}

module.exports = RoleName;
