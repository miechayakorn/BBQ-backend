"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Cronjoblog extends Model {
  static get primaryKey() {
    return "c_id";
  }
}

module.exports = Cronjoblog;
