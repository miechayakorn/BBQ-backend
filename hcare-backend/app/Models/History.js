"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class History extends Model {
  static get primaryKey() {
    return "h_id";
  }
}

module.exports = History;
