"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class HealthcareRole extends Model {
  static get primaryKey() {
    return "role_id";
  }
}

module.exports = HealthcareRole;
