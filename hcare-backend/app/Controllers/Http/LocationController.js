"use strict";
const CheckRoleAdmin = use("App/Service/CheckRole");
const Database = use("Database");
const Location = use("App/Models/Location");
const ServiceType = use("App/Models/ServiceType");
const moment = require("moment");
moment.locale();

class LocationController {
  async getLocations({ response, auth }) {
    try {
      const account = await auth.getUser();
      if (await CheckRoleAdmin.checkAdmin(account.role, response)) {
        const location_arr = await Database.table("locations");
        if (location_arr.length > 0) {
          let res_arr = location_arr.map((location) => {
            let data = {
              location_id: location.location_id,
              location_name: location.location_name,
              is_active: location.is_active === 1 ? true : false,
            };
            return data;
          });
          return res_arr;
        } else {
          return response.status(204).send();
        }
      }
      return response.status(403).send();
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async createLocation({ request, response, auth }) {
    try {
      const { location_name } = request.only(["location_name"]);
      const account = await auth.getUser();

      if (account.role != "ADMIN") {
        return response.status(403).send();
      } else {
        const old_location = await Database.select("*")
          .from("locations")
          .where({ location_name: location_name.trim() });
        if (old_location.length < 1) {
          await Location.create({
            location_name: location_name,
            is_active: 1,
            add_id: account.account_id,
          });
          return response.status(201).send("success");
        } else {
          response.send("duplicate location");
        }
      }
    } catch (error) {
      response.status(500).send(error);
    }
  }

  async updateLocationStatus({ request, response, auth }) {
    try {
      const { location_id, status } = request.only(["location_id", "status"]);
      const account = await auth.getUser();
      if (account.role != "ADMIN") {
        response.status(403).send();
      } else {
        const statusInDb = status === true ? 1 : 0;
        const availability = status === true ? "AVAILABLE" : "UNAVAILABLE";
        await Location.query()
          .where({ location_id: location_id })
          .update({ is_active: statusInDb, edit_id: account.account_id });

        await ServiceType.query().where({ location_id: location_id }).update({
          availability: availability,
          edit_id: account.account_id,
        });

        await Database.table("servicetypes")
          .innerJoin("work_times", "work_times.type_id", "servicetypes.type_id")
          .innerJoin("bookings", "work_times.working_id", "bookings.working_id")
          .whereRaw(
            `servicetypes.location_id = '${location_id}' AND bookings.date >= '${moment().format(
              "YYYY-MM-DD"
            )}'`
          )
          .update({
            "work_times.availability": availability,
            "work_times.edit_id": account.account_id,
            "work_times.updated_at": moment().format("YYYY-MM-DD HH:mm:ss"),
            "bookings.availability": availability,
            "bookings.edit_id": account.account_id,
          });

        return response.status(201).send("success");
      }
    } catch (error) {
      response.status(500).send(error);
    }
  }

  async deleteLocation({ response, auth, params }) {
    try {
      const account = await auth.getUser();
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const { location_id } = params;
        const location = await Location.find(location_id);
        if (location) {
          await location.delete();
          return response.status(200).send("delete location success");
        } else {
          return response.status(204).send();
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }
}

module.exports = LocationController;
