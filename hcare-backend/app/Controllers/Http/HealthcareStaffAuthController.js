"use strict";

const Database = use("Database");
const Account = use("App/Models/Account");
const Mail = use("Mail");
const Hash = use("Hash");
const Env = use("Env");
const Token = use("App/Models/Token");
const CheckMail = use("App/Service/CheckMail");

class HealthcareStaffAuthController {
  async createStaff({ request, response }) {
    const data = await request.only([
      "staff_id",
      "email",
      "password",
      "telephone",
      "first_name",
      "last_name",
      "role_in_healthcare",
    ]);
    //console.log(data);
    try {
      const checkMail = await CheckMail.checkMailKMUTT(data.email);
      if (checkMail == false) {
        return response
          .status(203)
          .send("กรุณากรอก @mail.kmutt.ac.th เท่านั้น");
      }

      const staff = await Account.create({
        hn_number: data.staff_id,
        email: data.email,
        password: data.password,
        telephone: data.telephone,
        first_name: data.first_name,
        last_name: data.last_name,
        role: "STAFF",
        role_in_healthcare: data.role_in_healthcare,
        verify: "NOT VERIFY",
      });

      if (staff) {
        const token = `${Date.now()}${staff.$attributes.hn_number}`;
        const tokenHash = await Hash.make(token);

        const dataForSendEmail = {
          staff: await Database.table("accounts")
            .where("account_id", staff.$attributes.account_id)
            .first(),
          tokenHash,
          url: Env.get("VUE_APP_BACKEND_URL"),
        };

        const sendMail = await Mail.send(
          "staffactivateaccount",
          dataForSendEmail,
          (message) => {
            message
              .to(dataForSendEmail.staff.email)
              .from(Env.get("MAIL_USERNAME"))
              .subject("Activate Staff Account");
          }
        );

        if (sendMail) {
          await Token.create({
            account_id: dataForSendEmail.staff.account_id,
            token: tokenHash,
            type: "STAFF REGISTER",
          });
          return response.send("sendmail success");
        }
      }
    } catch (err) {
      console.log(err);
      return response.status(err.status).send(err);
    }
  }

  // confirm after click in email
  async confirmRegister({ request, response }) {
    const query = request.get();
    try {
      if (query.token) {
        const accountConfirm = await Token.findBy("token", query.token);
        if (accountConfirm) {
          await Account.query()
            .where("account_id", accountConfirm.account_id)
            .update({ verify: "SUCCESS" });
          await Token.query()
            .where({
              token: query.token,
              account_id: accountConfirm.account_id,
              type: "STAFF REGISTER",
            })
            .update({ is_revoked: 1 });

          response.redirect(`${Env.get("VUE_APP_FONTEND_URL")}/admin/login`);
          //   return response.json({
          //     message: "Registration confirmation successful",
          //   });
        } else {
          // return response.status(304).json({
          //   message: "This token is not available",
          // });
          response.redirect(`${Env.get("VUE_APP_FONTEND_URL")}/admin/login`);
        }
      } else {
        // return response.status(500).json({
        //   message: "Token not exist",
        // });
        response.redirect(`${Env.get("VUE_APP_FONTEND_URL")}/admin/login`);
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async staffLogin({ request, response, auth }) {
    const { email, password } = request.only(["email", "password"]);
    try {
      if (await auth.attempt(email, password)) {
        const staff = await Account.findBy("email", request.input("email"));
        const token = await auth.generate(staff);
        if (staff.role == "STAFF" || staff.role == "ADMIN") {
          let dataResp = {
            account_id: staff.account_id,
            first_name: staff.first_name,
            last_name: staff.last_name,
            role: staff.role,
            type: token.type,
            token: token.token,
            refreshToken: token.refreshToken,
            profile_picture: staff.profile_picture,
          };
          return response.send(dataResp);
        }
        return response.status(401).send("no permission");
      }
    } catch (error) {
      return response.status(error.status).send(error);
    }
  }

  async staffForgetPassword({ request, response }) {
    try {
      const { email } = request.only(["email"]);
      let checkEmail = await CheckMail.checkMailKMUTT(email);

      if (checkEmail) {
        const account = await Account.findBy("email", email);
        if (account) {
          if (account.role == "STAFF" || account.role == "ADMIN") {
            const token = `${Date.now()}${account.hn_number}`;
            const tokenHash = await Hash.make(token);
            account.token = tokenHash;
            account.url = `${Env.get(
              "VUE_APP_FONTEND_URL"
            )}/admin/forgetpassword`;

            const sendMail = await Mail.send(
              "forgetpassword",
              account.toJSON(),
              (message) => {
                message
                  .to(account.email)
                  .from(Env.get("MAIL_USERNAME"))
                  .subject(`${account.role} Forget Password`);
              }
            );
            const tokenDb = await Database.select("*")
              .from("tokens")
              .where({
                account_id: account.account_id,
                type: "STAFF FORGET PASSWORD",
                is_revoked: 0,
              })
              .first();
            if (tokenDb) {
              await Token.query()
                .where({ token_id: tokenDb.token_id })
                .update({ token: tokenHash });
            } else {
              await Token.create({
                account_id: account.account_id,
                token: tokenHash,
                type: "STAFF FORGET PASSWORD",
              });
            }

            return response.send("sendmail success");
          } else {
            return response.status(202).send("ไม่มีสิทธ์ในการเข้าถึง");
          }
        } else {
          return response.status(202).send(`ไม่พบผู้ใช้งาน ${email} ในระบบ`);
        }
      } else {
        return response
          .status(202)
          .send("กรุณากรอกอีเมล @mail.kmutt.ac.th หรือ @kmutt.ac.th เท่านั้น");
      }
    } catch (error) {
      console.log(error);
      return response.status(error).send(error);
    }
  }

  async staffConfirmForgetPassword({ request, response }) {
    try {
      const { token, password } = request.only(["token", "password"]);
      if (token) {
        const userToken = await Database.select("*")
          .from("tokens")
          .where({
            token: token,
            type: "STAFF FORGET PASSWORD",
            is_revoked: 0,
          })
          .first();
        if (userToken) {
          const hashPassword = await Hash.make(password);
          await Account.query()
            .where({ account_id: userToken.account_id })
            .update({ password: hashPassword });

          await Token.query()
            .where({ token_id: userToken.token_id })
            .update({ is_revoked: 1 });
          return response.status(200).send();
        } else {
          return response
            .status(202)
            .send("token นี้ถูกใช้งานไปแล้วหรือไม่มีอยู่ในระบบ");
        }
      } else {
        return response.status(202).send("ไม่พบ token");
      }
    } catch (error) {
      console.log(error);
      return response.status(error).send(error);
    }
  }
}

module.exports = HealthcareStaffAuthController;
