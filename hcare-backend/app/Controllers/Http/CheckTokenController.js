"use strict";
const Env = use("Env");
const CryptoJS = require("crypto-js");

class CheckTokenController {
  async check({ response, auth }) {
    try {
      const account = await auth.getUser();
      const key = Env.get("SECRET_KEY");
      let first_name = CryptoJS.AES.encrypt(account.first_name, key).toString();
      let last_name = CryptoJS.AES.encrypt(account.last_name, key).toString();
      let role = CryptoJS.AES.encrypt(account.role, key).toString();
      if (account) {
        return response.status(200).json({
          first_name: first_name,
          last_name: last_name,
          role: role,
        });
      } else {
        return response.status(401).send("invalid signature");
      }
    } catch (error) {
      response.status(error.status).send(error);
    }
  }
}

module.exports = CheckTokenController;
