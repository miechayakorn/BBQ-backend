"use strict";

const Booking = use("App/Models/Booking");
const Database = use("Database");
const DateFormat = use("App/Service/DateService");
const moment = require("moment");
moment.locale();
const History = use("App/Models/History");

class BookingController {
  //แสดงประเภทการนัดหมาย
  async showType({ response, params }) {
    try {
      const types = await Database.select("type_id", "type_name")
        .from("servicetypes")
        .innerJoin(
          "locations",
          "servicetypes.location_id",
          "locations.location_id"
        )
        .where({
          "servicetypes.location_id": params.location_id,
          "servicetypes.availability": "AVAILABLE",
          "locations.is_active": 1,
        });

      const checkTypes = await Database.select(
        "servicetypes.type_id",
        "type_name"
      )
        .from("servicetypes")
        .innerJoin(
          "locations",
          "servicetypes.location_id",
          "locations.location_id"
        )
        .innerJoin("work_times", "work_times.type_id", "servicetypes.type_id")
        .innerJoin("bookings", "bookings.working_id", "work_times.working_id")
        .whereRaw(
          `servicetypes.location_id = ${
            params.location_id
          } AND servicetypes.availability = "AVAILABLE" AND work_times.availability = "AVAILABLE" AND bookings.availability =  "AVAILABLE" AND locations.is_active = 1 AND bookings.date >= '${moment().format(
            "YYYY-MM-DD"
          )}'`
        )
        .groupByRaw("servicetypes.type_id , type_name");

      let dataRes = [];
      for (let i = 0; i < types.length; i++) {
        let findindex = checkTypes.findIndex(
          (data) => data.type_id === types[i].type_id
        );
        if (findindex == -1) {
          dataRes.push({
            type_id: types[i].type_id,
            type_name: types[i].type_name,
            checknull: false,
          });
        } else {
          dataRes.push({
            type_id: types[i].type_id,
            type_name: types[i].type_name,
            checknull: true,
          });
        }
      }
      return response.send(dataRes);
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  //แสดงวันที่ของนัดประเภทที่เลือกมีให้บริการ
  async showDate({ request, response, params }) {
    try {
      const { role } = request.only(["role"]);
      const condition =
        role === undefined
          ? `AND date <= '${moment().add(1, "M").format("YYYY-MM-DD")}'`
          : "";

      let allBooking = await Database.select("type_id", "date")
        .select(Database.raw('DATE_FORMAT(date, "%Y-%m-%d") as datevalue'))
        .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as dateformat'))
        .from("bookings")
        .innerJoin("work_times", "bookings.working_id", "work_times.working_id")
        .whereRaw(
          `type_id = '${
            params.type_id
          }' AND date >= '${new Date()
            .toISOString()
            .slice(
              0,
              10
            )}' AND work_times.availability= "AVAILABLE" ${condition}`
        )
        .groupByRaw("type_id, date, datevalue, dateformat")
        .orderBy("bookings.date");

      for (let index = 0; index < allBooking.length; index++) {
        allBooking[index].dateformat = DateFormat.ChangeDateFormat(
          allBooking[index].dateformat
        );
      }
      return response.send(allBooking);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //แสดงช่วงเวลาที่นัดประเภทที่เลือกและวันที่เลือกมีให้บริการ
  async showTime({ request, response }) {
    try {
      const { time, working_id } = request.only(["time", "working_id"]);
      const date = time;
      let array_time = [];
      if (date == moment().format("YYYY-MM-DD")) {
        array_time = await Database.select(
          "booking_id",
          "type_id",
          "date",
          "time_in",
          "status",
          "bookings.availability AS availability"
        )
          .from("bookings")
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .whereRaw(
            `bookings.working_id = ${working_id}
             AND date = '${date}' 
             AND time_in >= '${moment().format("HH:mm:ss")}'`
          )
          .orderBy("time_in");
      } else {
        array_time = await Database.select(
          " booking_id",
          "type_id",
          "date",
          "time_in",
          "status",
          "bookings.availability AS availability"
        )
          .from("bookings")
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .whereRaw(
            `bookings.working_id = ${working_id}
             AND date = '${date}'`
          )
          .orderBy("time_in");
      }
      return response.send(array_time);
    } catch (error) {
      return error;
    }
  }

  async showDoctor({ request, response }) {
    try {
      const { type_id, date } = request.only(["type_id", "date"]);
      const doctors = await Database.select(
        "bookings.working_id",
        "work_times.doctor_id",
        "accounts.prefix",
        "accounts.first_name",
        "accounts.last_name",
        "accounts.profile_picture"
      )
        .from("work_times")
        .innerJoin("accounts", "work_times.doctor_id", "accounts.account_id")
        .innerJoin("bookings", "work_times.working_id", "bookings.working_id")
        .where({
          type_id: type_id,
          day: moment(new Date(date).toISOString().slice(0, 10))
            .format("dddd")
            .toUpperCase(),
          date: date,
        })
        .groupByRaw(
          "bookings.working_id , work_times.doctor_id, accounts.prefix, accounts.first_name, accounts.last_name"
        );
      let resData = doctors.map((doc) => {
        return {
          working_id: doc.working_id,
          doctor_id: doc.doctor_id,
          doctor_name: `${doc.prefix ? doc.prefix : ""} ${doc.first_name} ${
            doc.last_name
          }`,
          doctor_profile_picture: doc.profile_picture,
        };
      });
      return response.status(200).send(resData);
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  async submitBooking({ request, response, auth }) {
    try {
      const dataFromBooking = request.only(["booking_id", "symptom"]);
      const account = await auth.getUser();

      if (account.verify == "NOT VERIFY") {
        return response.json({ message: "Please verrify account" });
      }

      const check_unvailability = await Database.select("*")
        .from("bookings")
        .where("booking_id", dataFromBooking.booking_id)
        .first();

      if (check_unvailability.availability == "UNAVAILABLE") {
        return response
          .status(203)
          .send("ตารางนัดหมายที่คุณเลือกปิดให้บริการอยู่");
      }

      const booking = await Booking.find(dataFromBooking.booking_id);
      const bkDate = moment(booking.date).format("YYYY-MM-DD");
      const check_day_booking = await Database.select("*")
        .from("bookings")
        .whereRaw(
          `account_id_from_user = '${account.account_id}' AND working_id = '${booking.working_id}' AND status = 'CONFIRM SUCCESS' AND date = '${bkDate}'`
        );

      if (check_day_booking.length > 0) {
        return response
          .status(203)
          .send(`คุณไม่สามารถจองนัดหมายซ้ำภายในวันเดียวกันได้`);
      }

      const timeOut = moment(
        `${new Date().toISOString().slice(0, 10)} ${booking.time_out}`
      )
        .add(-1, "minutes")
        .format("HH:mm:ss");

      const check_booking_by_date = await Database.select("*")
        .from("bookings")
        .whereRaw(
          `account_id_from_user = '${account.account_id}' AND status = 'CONFIRM SUCCESS' AND date = '${bkDate}' AND (time_in BETWEEN '${booking.time_in}' AND '${timeOut}')`
        )
        .union([
          Database.select("*")
            .from("bookings")
            .whereRaw(
              `account_id_from_user = '${account.account_id}' AND status = 'CONFIRM SUCCESS' AND date = '${bkDate}' AND (time_out BETWEEN '${booking.time_in}' AND '${timeOut}')`
            ),
        ]);

      if (check_booking_by_date.length > 0) {
        return response
          .status(203)
          .send(
            `คุณไม่สามารถจองนัดในช่วงเวลานี้ได้ เนื่องจากเวลาซ้ำกับนัดหมายที่คุณมีอยู่`
          );
      }

      if (account) {
        const checkBookngStatus = await Booking.find(
          dataFromBooking.booking_id
        );
        if (checkBookngStatus.status == "CONFIRM SUCCESS") {
          return response.status(203).send("ขออภัยเวลาที่คุณไม่สามารถจองได้");
        }

        await Booking.query()
          .where({ booking_id: dataFromBooking.booking_id, status: null })
          .update({
            status: "CONFIRM SUCCESS",
            comment_from_user: dataFromBooking.symptom,
            account_id_from_user: account.account_id,
          });

        let booking_details = await Database.select(
          "locations.location_name",
          "servicetypes.type_name",
          "bookings.booking_id",
          "bookings.date",
          "bookings.time_in",
          "bookings.time_out",
          "bookings.comment_from_user",
          "bookings.comment_from_staff",
          "bookings.account_id_from_user",
          "accounts.prefix",
          Database.raw(
            "CONCAT(accounts.first_name,' ',accounts.last_name) as doctor_name"
          )
        )
          .from("bookings")
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .innerJoin(
            "servicetypes",
            "work_times.type_id",
            "servicetypes.type_id"
          )
          .innerJoin(
            "locations",
            "servicetypes.location_id",
            "locations.location_id"
          )
          .innerJoin("accounts", "work_times.doctor_id", "accounts.account_id")
          .where({ booking_id: dataFromBooking.booking_id })
          .first();

        await History.create({
          location: booking_details.location_name,
          type_name: booking_details.type_name,
          booking_id: booking_details.booking_id,
          date: booking_details.date,
          time_in: booking_details.time_in,
          time_out: booking_details.time_out,
          comment_from_user: booking_details.comment_from_user,
          comment_from_staff: booking_details.comment_from_staff,
          account_id: booking_details.account_id_from_user,
          prefix_doctor: booking_details.prefix,
          doctor_name: booking_details.doctor_name,
          is_active: 1,
        });

        return response.status(201).send("success");
      } else {
        return response.status(401).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error.message);
    }
  }

  //ยินยันการนัดหมายหลังจากผู้ใช้กดยืนยันจาก e-mail
  async confirmBooking({ request, response }) {
    const query = request.get();
    try {
      if (query.token) {
        const booking = await Booking.findBy(
          "token_booking_confirm",
          query.token
        );

        if (booking) {
          await Booking.query().where("booking_id", booking.booking_id).update({
            status: "CONFIRM SUCCESS",
            token_booking_confirm: null,
          });

          let booking_details = await Database.select(
            "locations.location_name",
            "servicetypes.type_name",
            "bookings.booking_id",
            "bookings.date",
            "bookings.time_in",
            "bookings.time_out",
            "bookings.comment_from_user",
            "bookings.comment_from_staff",
            "bookings.account_id_from_user",
            "accounts.prefix",
            Database.raw(
              "CONCAT(accounts.first_name,' ',accounts.last_name) as doctor_name"
            )
          )
            .from("bookings")
            .innerJoin(
              "work_times",
              "bookings.working_id",
              "work_times.working_id"
            )
            .innerJoin(
              "servicetypes",
              "work_times.type_id",
              "servicetypes.type_id"
            )
            .innerJoin(
              "locations",
              "servicetypes.location_id",
              "locations.location_id"
            )
            .innerJoin(
              "accounts",
              "work_times.doctor_id",
              "accounts.account_id"
            )
            .where({ booking_id: booking.booking_id })
            .first();

          await History.create({
            location: booking_details.location_name,
            type_name: booking_details.type_name,
            booking_id: booking_details.booking_id,
            date: booking_details.date,
            time_in: booking_details.time_in,
            time_out: booking_details.time_out,
            comment_from_user: booking_details.comment_from_user,
            comment_from_staff: booking_details.comment_from_staff,
            account_id: booking_details.account_id_from_user,
            prefix_doctor: booking_details.prefix,
            doctor_name: booking_details.doctor_name,
            is_active: 1,
          });

          const bookingNew = await Booking.find(booking.booking_id);

          return response.json({
            message: "Booking confirmation successful",
            booking: bookingNew,
          });
        } else {
          return response.status(304).json({
            message: "This token is not available",
          });
        }
      } else {
        return response.status(500).send("Token not exist");
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async getLocation({ response }) {
    try {
      const location = await Database.select("*")
        .from("locations")
        .where({ is_active: 1 });
      if (location.length <= 0) {
        return response.status(204).send();
      } else {
        return response.send(location);
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async searchEmail({ request, response }) {
    try {
      const { q } = request.only(["q"]);
      const allMail = await Database.select(
        "email",
        "account_id",
        Database.raw(
          "(CASE WHEN prefix IS NOT NULL THEN CONCAT(prefix,' ',first_name,' ',last_name) ELSE CONCAT(first_name,' ',last_name) END) AS name"
        )
      )
        .from("accounts")
        .whereRaw(
          `email LIKE '%${q}%' AND is_active = '1' AND email != 'admin@mail.kmutt.ac.th' AND account_id  != '1'`
        );
      return response.send(allMail);
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  //จองตารางนัดหมาย และส่ง e-mail
  /*async submitBooking({ request, response, auth }) {
    try {
      const dataFromBooking = request.only(["booking_id", "symptom"]);
      const account = await auth.getUser();

      if (account.verify == "NOT VERIFY") {
        return response.json({ message: "Please verrify account" });
      }

      const check_unvailability = await Database.select("*")
        .from("bookings")
        .where("booking_id", dataFromBooking.booking_id)
        .first();

      if (check_unvailability.availability == "UNAVAILABLE") {
        return response.status(500).send("this booking is unavailable");
      }

      // find booking slot from bookingID that get from request to find in DB
      let findBooking = await Database.select(
        "booking_id",
        "type_name",
        "time_in",
        "time_out",
        "date",
        "status"
      )
        .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as date'))
        .from("bookings")
        .innerJoin("work_times", "bookings.working_id", "work_times.working_id")
        .innerJoin("servicetypes", "work_times.type_id", "servicetypes.type_id")
        .where("bookings.booking_id", dataFromBooking.booking_id)
        .first();

      findBooking.date = DateFormat.ChangeDateFormat(findBooking.date);

      if (account) {
        if (!findBooking.status) {
          const tokenNoHash = `${Date.now()}${
            findBooking.booking_id
          }${Date.now()}`;
          const token = await Hash.make(tokenNoHash);

          const dataForSendEmail = {
            account: account,
            bookingSlot: findBooking,
            token,
            url: Env.get("VUE_APP_FONTEND_URL"),
          };

          const subject =
            "Submit Booking From Health Care  " +
            dataForSendEmail.bookingSlot.type_name.toString();

          await Mail.send("confirmbooking", dataForSendEmail, (message) => {
            message
              .to(account.email)
              .from(Env.get("MAIL_USERNAME"))
              .subject(subject);
          });

          await Booking.query()
            .where("booking_id", dataFromBooking.booking_id)
            .update({
              account_id_from_user: dataForSendEmail.account.account_id,
              status: "WAITING CONFIRM",
              comment_from_user: dataFromBooking.symptom,
              token_booking_confirm: token,
            });
          return "send mail success";
        }
        return response.status(400).send("This booking unavailable");
      }
    } catch (error) {
      console.log(error);
      return response.status(500).send(error);
    }
  }*/
}

module.exports = BookingController;
