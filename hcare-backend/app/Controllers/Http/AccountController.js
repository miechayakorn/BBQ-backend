"use strict";

const { get } = require("@adonisjs/framework/src/Route/Manager");

const Database = use("Database");
const Account = use("App/Models/Account");
const Booking = use("App/Models/Booking");
const History = use("App/Models/History");
const { validate } = use("Validator");
const CheckHn = use("App/Service/CheckHN");

class AccountController {
  async uploadProfilePicture({ request, response, auth }) {
    try {
      const { profile_picture, account_id } = await request.only([
        "profile_picture",
        "account_id",
      ]);
      const account = await auth.getUser();
      const account_user = await Account.find(account_id);
      if (account && account_user) {
        await Account.query()
          .where({
            account_id: account_user.account_id,
            is_active: 1,
          })
          .update({
            profile_picture: profile_picture,
            edit_id: account.account_id,
          });
        return response.status(200).send("upload profile picture sucess");
      } else {
        response.status(401).send();
      }
    } catch (error) {
      return response.status(error.status).send(error.message);
    }
  }

  async getUserProfile({ response, auth }) {
    try {
      const account = await auth.getUser();
      const profile = await Account.find(account.account_id);
      return response.status(200).json({
        account_id: profile.account_id,
        first_name: profile.first_name,
        last_name: profile.last_name,
        email: profile.email,
        telephone: profile.telephone,
        profile_picture: profile.profile_picture,
        hn_number: profile.hn_number,
        role: profile.role,
      });
    } catch (error) {
      return response.status(error.status).send(error.message);
    }
  }

  async saveEditProfile({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const {
        first_name,
        last_name,
        telephone,
        profile_picture,
        hn_number,
      } = await request.only([
        "first_name",
        "last_name",
        "telephone",
        "profile_picture",
        "hn_number",
      ]);

      const validation = await validate(
        { first_name, last_name, telephone, hn_number },
        {
          first_name: "required",
          last_name: "required",
          telephone: "required",
          hn_number: "required",
        }
      );

      if (validation.fails()) {
        return response.send(validation.messages());
      }

      const checkHn = await CheckHn.checkHnInSystem(hn_number);
      const accountInSys = await Account.find(account.account_id);
      if (checkHn == false && hn_number != accountInSys.hn_number) {
        return response.status(203).send("รหัสพนักงานซ้ำกับที่มีในระบบ");
      }

      await Account.query()
        .where({
          account_id: account.account_id,
          is_active: 1,
        })
        .update({
          first_name: first_name,
          last_name: last_name,
          telephone: telephone,
          profile_picture: profile_picture,
          hn_number: hn_number,
          edit_id: account.account_id,
        });

      return response.status(200).send("update profile success");
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error.message);
    }
  }
}

module.exports = AccountController;
