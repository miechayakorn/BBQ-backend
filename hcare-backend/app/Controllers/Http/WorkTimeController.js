"use strict";

const WorkTime = use("App/Models/WorkTime");
const Booking = use("App/Models/Booking");
const Database = use("Database");
const moment = require("moment");
moment.locale();
const { validate } = use("Validator");
const DateService = use("App/Service/DateService");
const History = use("App/Models/History");

class WorkTimeController {
  async addWorkTime({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const {
        type_id,
        day,
        account_id,
        start_time,
        end_time,
        time_slot,
      } = request.only([
        "type_id",
        "day",
        "account_id",
        "start_time",
        "end_time",
        "time_slot",
      ]);
      if (account.role == "STAFF" || account.role == "ADMIN") {
        if (
          type_id != null &&
          day != null &&
          account_id != null &&
          start_time != null &&
          end_time != null &&
          time_slot != null
        ) {
          const check_worktimes = await Database.select("*")
            .from("work_times")
            .where({ doctor_id: account_id, day: day });
          if (check_worktimes.length <= 0) {
            await WorkTime.create({
              time_slot: time_slot,
              start_time: start_time,
              end_time: end_time,
              day: day,
              availability: "AVAILABLE",
              type_id: type_id,
              doctor_id: account_id,
              add_id: account.account_id,
            });
            response.status(201).send("success");
          } else {
            return response.status(200).send("duplicate worktime");
          }
        } else {
          return response.status(200).send("parameter missing");
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async getDayOfWorks({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const { type_id, account_id } = request.only(["type_id", "account_id"]);
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const dayOfWork = await Database.select("working_id", "day")
          .from("work_times")
          .where({ type_id: type_id, doctor_id: account_id })
          .orderBy("day");
        if (dayOfWork.length > 0) {
          let dayOfWorkTH = dayOfWork.map((day_date) => {
            let dayName = `${day_date.day.charAt(0)}${day_date.day
              .substr(1, day_date.day.length - 1)
              .toLowerCase()}`;
            return {
              working_id: day_date.working_id,
              day: day_date.day,
              วันที่: DateService.changeDay(dayName),
            };
          });
          return dayOfWorkTH;
        } else {
          return response.status(204).send();
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async getWorkTimeDetail({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const { working_id } = await request.only(["working_id"]);
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const workTime = await Database.select(
          "servicetypes.location_id",
          "locations.location_name",
          "work_times.type_id",
          "servicetypes.type_name",
          "working_id",
          "time_slot",
          "start_time",
          "end_time",
          "day",
          "work_times.availability",
          "doctor_id",
          "prefix",
          "first_name",
          "last_name"
        )
          .from("work_times")
          .innerJoin("accounts", "work_times.doctor_id", "accounts.account_id")
          .innerJoin(
            "servicetypes",
            "work_times.type_id",
            "servicetypes.type_id"
          )
          .innerJoin(
            "locations",
            "servicetypes.location_id",
            "locations.location_id"
          )
          .where({ working_id: working_id })
          .first();
        if (workTime != null) {
          return response.json({
            location_id: workTime.location_id,
            location_name: workTime.location_name,
            type_id: workTime.type_id,
            type_name: workTime.type_name,
            working_id: workTime.working_id,
            time_slot: workTime.time_slot,
            start_time: workTime.start_time,
            end_time: workTime.end_time,
            day: workTime.day,
            วันที่: DateService.changeDay(
              `${workTime.day.charAt(0)}${workTime.day
                .substr(1, workTime.day.length - 1)
                .toLowerCase()}`
            ),
            status: workTime.availability == "AVAILABLE" ? true : false,
            type_id: workTime.type_id,
            doctor_id: workTime.doctor_id,
            prefix: workTime.prefix,
            first_name: workTime.first_name,
            last_name: workTime.last_name,
          });
        } else {
          return response.status(204).send();
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async updateWorktime({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const {
        working_id,
        start_time,
        end_time,
        time_slot,
        account_id,
      } = request.only([
        "working_id",
        "start_time",
        "end_time",
        "time_slot",
        "account_id",
      ]);
      if (account.role == "ADMIN") {
        const validation = await validate(
          { working_id, start_time, end_time, time_slot, account_id },
          {
            working_id: "required",
            start_time: "required",
            end_time: "required",
            time_slot: "required",
            account_id: "required",
          }
        );
        if (validation.fails()) {
          return response.send(validation.messages());
        }
        await WorkTime.query().where({ working_id: working_id }).update({
          time_slot: time_slot,
          start_time: start_time,
          end_time: end_time,
          doctor_id: account_id,
          edit_id: account.account_id,
        });
        return response.status(201).send("success");
      } else if (account.role == "STAFF") {
        const validation = await validate(
          { working_id, start_time, end_time, time_slot },
          {
            working_id: "required",
            start_time: "required",
            end_time: "required",
            time_slot: "required",
          }
        );
        if (validation.fails()) {
          return response.send(validation.messages());
        }
        await WorkTime.query().where({ working_id: working_id }).update({
          time_slot: time_slot,
          start_time: start_time,
          end_time: end_time,
          edit_id: account.account_id,
        });
        return response.status(201).send("success");
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async updateWorktimeStatus({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const { working_id, status } = request.only(["working_id", "status"]);
      if (account.role == "STAFF" || account.role == "ADMIN") {
        let availability_db = status == true ? "AVAILABLE" : "UNAVAILABLE";
        const wt = await WorkTime.find(working_id);
        if (wt) {
          const checkWtBk = await Booking.query()
            .where({
              working_id: working_id,
            })
            .fetch();
          if (checkWtBk.toJSON().length > 0) {
            await Database.table("work_times")
              .innerJoin(
                "bookings",
                "work_times.working_id",
                "bookings.working_id"
              )
              .whereRaw(
                `work_times.working_id = '${working_id}' AND bookings.working_id = '${working_id}' AND bookings.date >= '${moment().format(
                  "YYYY-MM-DD"
                )}'`
              )
              .update({
                "work_times.availability": availability_db,
                "work_times.edit_id": account.account_id,
                "work_times.updated_at": moment().format("YYYY-MM-DD HH:mm:ss"),
                "bookings.availability": availability_db,
                "bookings.edit_id": account.account_id,
              });
          } else {
            await Database.table("work_times")
              .where({ working_id: working_id })
              .update({
                "work_times.availability": availability_db,
                "work_times.edit_id": account.account_id,
                "work_times.updated_at": moment().format("YYYY-MM-DD HH:mm:ss"),
              });
          }
          return response.status(201).send("success");
        } else {
          return response.status(200).send("Don't have this worktime");
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  async deleteWorktimes({ response, auth, params }) {
    try {
      const account = await auth.getUser();
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const { working_id } = params;
        const work_times = await WorkTime.find(working_id);
        if (work_times) {
          let bookings = await Booking.query()
            .where({ working_id: working_id })
            .fetch();
          if (bookings) {
            bookings = bookings.toJSON();
            bookings.forEach(async (booking) => {
              await History.query()
                .whereRaw(
                  `booking_id = '${booking.booking_id}' AND is_active = '1'`
                )
                .update({ is_active: 0 });
            });
          }
          await work_times.delete();
          return response.status(200).send("delete worktime success");
        } else {
          return response.status(204).send();
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(500).send(error);
    }
  }

  async getServiceResponsiblePerson({ request, response, auth }) {
    try {
      const { type_id } = request.only(["type_id"]);
      const account = await auth.getUser();
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const condition =
          account.role == "STAFF"
            ? `AND doctor_id = '${account.account_id}'`
            : "";
        const responsiblePerson = await Database.select("*")
          .from("work_times")
          .innerJoin("accounts", "work_times.doctor_id", "accounts.account_id")
          .whereRaw(`type_id = ${type_id} ${condition}`);
        let res_data = responsiblePerson.map((person) => {
          let data = {
            วันที่ให้บริการ:
              "วัน" +
              DateService.changeDay(
                `${person.day.charAt(0)}${person.day
                  .substring(1)
                  .toLowerCase()}`
              ),
            เวลาให้บริการ: `${person.start_time.substring(
              0,
              5
            )} - ${person.end_time.substring(0, 5)}`,
            ผู้รับผิดชอบ: `${person.prefix ? person.prefix : ""} ${
              person.first_name
            } ${person.last_name}`,
            working_id: person.working_id,
          };
          return data;
        });
        return response.status(200).send(res_data);
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  async getWorkTimePerson({ request, response, auth }) {
    try {
      const { account_id, type_id } = request.only(["account_id", "type_id"]);
      const account = await auth.getUser();
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const worktimeDetail = await Database.select("*")
          .from("work_times")
          .innerJoin(
            "servicetypes",
            "work_times.type_id",
            "servicetypes.type_id"
          )
          .innerJoin("accounts", "work_times.doctor_id", "accounts.account_id")
          .where({
            "work_times.doctor_id": account_id,
            "work_times.type_id": type_id,
          });
        let res_data;

        for (let i = 0; i < worktimeDetail.length; i++) {
          if (!res_data) {
            res_data = {
              type_name: worktimeDetail[i].type_name,
              doctor: `${
                worktimeDetail[i].prefix ? worktimeDetail[i].prefix : ""
              } ${worktimeDetail[i].first_name} ${worktimeDetail[i].last_name}`,
              worktime: [
                {
                  day: DateService.changeDay(
                    `${worktimeDetail[i].day.charAt(0)}${worktimeDetail[i].day
                      .substring(1)
                      .toLowerCase()}`
                  ),
                  time: `${worktimeDetail[i].start_time.substring(
                    0,
                    5
                  )} - ${worktimeDetail[i].end_time.substring(0, 5)}`,
                },
              ],
            };
          } else {
            let data = {
              day: DateService.changeDay(
                `${worktimeDetail[i].day.charAt(0)}${worktimeDetail[i].day
                  .substring(1)
                  .toLowerCase()}`
              ),
              time: `${worktimeDetail[i].start_time.substring(
                0,
                5
              )} - ${worktimeDetail[i].end_time.substring(0, 5)}`,
            };
            res_data.worktime.push(data);
          }
        }
        return response.status(200).send(res_data);
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }
}

module.exports = WorkTimeController;
