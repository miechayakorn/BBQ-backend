"use strict";
const Database = use("Database");
const DateService = use("App/Service/DateService");
const moment = require("moment");
moment.locale();

class HistoryController {
  async getHistoryForStaff({ request, response, auth }) {
    try {
      const { account_id } = request.only(["account_id"]);
      const staff = await auth.getUser();
      if (staff.role == "STAFF" || staff.role == "ADMIN") {
        const userHistory = await Database.raw(
          `SELECT histories.booking_id, accounts.account_id, accounts.hn_number, 
          CONCAT(accounts.first_name,' ',accounts.last_name) as name, histories.date,
          histories.time_in, histories.time_out, histories.comment_from_user as symptom,
          histories.comment_from_staff, histories.type_name, histories.prefix_doctor as prefix,
          histories.doctor_name, histories.is_active  FROM histories INNER JOIN accounts ON
          histories.account_id = accounts.account_id
          WHERE histories.account_id = ${account_id}`
        );
        const res_data = userHistory[0].map((his) => {
          let data = {
            booking_id: his.booking_id,
            account_id: his.account_id,
            hn_number: his.hn_number,
            name: his.name,
            date: DateService.ChangeDateFormat(
              moment(his.date).format("dddd D MMMM YYYY")
            ),
            time_in: his.time_in,
            time_out: his.time_out,
            symptom: his.symptom,
            comment_from_staff: his.comment_from_staff,
            type_name: his.type_name,
            prefix: his.prefix,
            doctor_name: his.doctor_name,
            is_active: his.is_active,
          };
          return data;
        });
        //console.log(res_data);
        return response.status(200).send(res_data);
      }
      return response.status(403).send();
    } catch (error) {
      response.status(500).send(error);
    }
  }

  async getHistoryForUser({ request, response, auth }) {
    try {
      const user = await auth.getUser();
      const userHistory = await Database.raw(
        `SELECT histories.h_id, histories.booking_id, accounts.account_id, accounts.hn_number, 
          CONCAT(accounts.first_name,' ',accounts.last_name) as name, histories.date,
          histories.time_in, histories.time_out, histories.comment_from_user as symptom,
          histories.comment_from_staff, histories.type_name, histories.prefix_doctor as prefix,
          histories.doctor_name, histories.is_active  FROM histories INNER JOIN accounts ON
          histories.account_id = accounts.account_id
          WHERE histories.account_id = ${user.account_id} ORDER BY h_id DESC`
      );
      const res_data = userHistory[0].map((his) => {
        let data = {
          h_id: his.h_id,
          booking_id: his.booking_id,
          account_id: his.account_id,
          hn_number: his.hn_number,
          name: his.name,
          date: DateService.ChangeDateFormat(
            moment(his.date).format("dddd D MMMM YYYY")
          ),
          time_in: his.time_in,
          time_out: his.time_out,
          symptom: his.symptom,
          type_name: his.type_name,
          prefix: his.prefix,
          doctor_name: his.doctor_name,
          is_active: his.is_active,
        };
        return data;
      });
      //console.log(res_data);
      return response.status(200).send(res_data);
    } catch (error) {
      response.status(500).send(error);
    }
  }
  
  async getHistoryDetail({ request, response, auth, params }) {
    try {
      const user = await auth.getUser();
      const userHistory = await Database.raw(
        `SELECT histories.h_id, histories.booking_id, accounts.account_id, accounts.hn_number, 
          CONCAT(accounts.first_name,' ',accounts.last_name) as name, histories.date,
          histories.time_in, histories.time_out, histories.comment_from_user as symptom,
          histories.comment_from_staff, histories.type_name, histories.prefix_doctor as prefix,
          histories.doctor_name, histories.is_active  FROM histories INNER JOIN accounts ON
          histories.account_id = accounts.account_id
          WHERE histories.booking_id = ${params.booking_id} AND histories.account_id = ${user.account_id} ORDER BY h_id DESC`
      );
      const res_data = userHistory[0].map((his) => {
        let data = {
          h_id: his.h_id,
          booking_id: his.booking_id,
          account_id: his.account_id,
          hn_number: his.hn_number,
          name: his.name,
          date: DateService.ChangeDateFormat(
            moment(his.date).format("dddd D MMMM YYYY")
          ),
          time_in: his.time_in,
          time_out: his.time_out,
          symptom: his.symptom,
          type_id: his.type_id,
          type_name: his.type_name,
          prefix: his.prefix,
          doctor_name: his.doctor_name,
          is_active: his.is_active,
        };
        return data;
      });
      return response.status(200).send(res_data[0]);
    } catch (error) {
      response.status(500).send(error);
    }
  }
}

module.exports = HistoryController;
