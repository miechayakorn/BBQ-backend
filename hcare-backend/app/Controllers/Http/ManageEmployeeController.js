"use strict";
const Database = use("Database");
const CheckRole = use("App/Service/CheckRole");
const WorkTime = use("App/Models/WorkTime");
const History = use("App/Models/History");
const Booking = use("App/Models/Booking");
const { validate } = use("Validator");
const Account = use("App/Models/Account");
const Mail = use("Mail");
const Hash = use("Hash");
const Env = use("Env");
const Token = use("App/Models/Token");
const CheckMail = use("App/Service/CheckMail");
const CheckHn = use("App/Service/CheckHN");

class ManageEmployeeController {
  async createEmployee({ request, response, auth }) {
    try {
      const { email, first_name, last_name, hn_number } = request.only([
        "email",
        "first_name",
        "last_name",
        "hn_number",
      ]);

      const admin = await auth.getUser();

      const validation = await validate(
        { first_name, last_name, email, hn_number },
        {
          first_name: "required",
          last_name: "required",
          email: "required",
          hn_number: "required",
        }
      );

      if (validation.fails()) {
        return response.send(validation.messages());
      }

      if (admin.role == "ADMIN") {
        if ((await CheckMail.checkMailKMUTT(email)) == false) {
          return response
            .status(203)
            .send("กรุณากรอก @mail.kmutt.ac.th เท่านั้น");
        }

        const search_email = await Database.table("accounts")
          .where("email", email)
          .first();

        if (search_email) {
          return response.status(203).send("email ซ้ำกับที่มีในระบบ");
        } else {
          const checkHn = await CheckHn.checkHnInSystem(hn_number);
          if (checkHn == false) {
            return response.status(203).send("รหัสพนักงานซ้ำกับที่มีในระบบ");
          }
          let new_emp = await Account.create({
            first_name: first_name,
            last_name: last_name,
            hn_number: hn_number,
            verify: "NOT VERIFY",
            email: email,
            role: "STAFF",
            add_id: admin.account_id,
            is_active: 1,
          });
          new_emp = new_emp.toJSON();

          if (new_emp) {
            let token = `${Date.now()}${new_emp.hn_number}`;
            token = await Hash.make(token);
            const dataForSendEmail = {
              employee: await Database.table("accounts")
                .where("account_id", new_emp.account_id)
                .first(),
              token: token,
              url: Env.get("VUE_APP_FONTEND_URL"),
            };

            const sendMail = await Mail.send(
              "staffactivateaccount",
              dataForSendEmail,
              (message) => {
                message
                  .to(dataForSendEmail.employee.email)
                  .from(Env.get("MAIL_USERNAME"))
                  .subject("Activate Staff Account");
              }
            );

            // const token_db = await Database.select("*")
            //   .from("tokens")
            //   .where({ account_id: dataForSendEmail.employee.account_id })
            //   .first();

            //if (token_db == undefined || token_db == null) {
            await Token.create({
              account_id: dataForSendEmail.employee.account_id,
              token: token,
              type: "STAFF REGISTER",
            });
            //} else {
            //   await Token.query()
            //     .where({ token_id: token_db.token_id })
            //     .update({ token: token });
            // }
            return response.status(201).send("sendmail success");
          } else {
            return response.send("can not create staff");
          }
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  async getNewEmployee({ request, response }) {
    try {
      const { token, account_id } = request.only(["token", "account_id"]);
      const validation = await validate(
        { token, account_id },
        { token: "required", account_id: "required" }
      );

      if (validation.fails()) {
        return response.send(validation.messages());
      }

      const check_from_token = await Database.select("*")
        .from("tokens")
        .where({ token: token, is_revoked: 0 })
        .first();

      const new_emp = await Database.select("*")
        .from("accounts")
        .where({ account_id: account_id, verify: "NOT VERIFY", is_active: 1 })
        .first();

      if (new_emp && check_from_token.account_id == new_emp.account_id) {
        return response.json({
          account_id: new_emp.account_id,
          email: new_emp.email,
          first_name: new_emp.first_name,
          last_name: new_emp.last_name,
          role: new_emp.role,
          hn_number: new_emp.hn_number,
        });
      } else {
        return response.status(202).send("already registered");
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async savePersonalDataEmployee({ request, response }) {
    try {
      const {
        account_id,
        prefix,
        first_name,
        last_name,
        hn_number,
        email,
        telephone,
        password,
      } = await request.only([
        "account_id",
        "prefix",
        "first_name",
        "last_name",
        "hn_number",
        "email",
        "telephone",
        "password",
      ]);
      const validation = await validate(
        {
          account_id,
          prefix,
          first_name,
          last_name,
          hn_number,
          email,
          telephone,
          password,
        },
        {
          account_id: "required|number",
          prefix: "required",
          first_name: "required",
          last_name: "required",
          hn_number: "required|number",
          email: "required",
          telephone: "required",
          password: "required",
        }
      );
      if (validation.fails()) {
        return response.send(validation.messages());
      }

      const checkHn = await CheckHn.checkHnInSystem(hn_number);
      const accountInSystem = await Account.query()
        .where({ email: email })
        .first();
      if (checkHn == false && email != accountInSystem.email) {
        return response.status(203).send("รหัสพนักงานซ้ำกับที่มีในระบบ");
      }

      const encrypt_password = await Hash.make(password);
      const new_emp = await Account.query()
        .where("account_id", account_id)
        .update({
          prefix: prefix,
          first_name: first_name,
          last_name: last_name,
          hn_number: hn_number,
          telephone: telephone,
          password: encrypt_password,
          verify: "SUCCESS",
          edit_id: account_id,
        });

      await Token.query()
        .where({
          account_id: account_id,
          type: "STAFF REGISTER",
          is_revoked: "0",
        })
        .update({ is_revoked: 1 });

      return response.status(201).send("create success");
    } catch (error) {
      console.log(error);
      response.status(500).send(error);
    }
  }

  async getOnlyoneEmployeeData({ request, response, auth }) {
    try {
      const { account_id } = request.only(["account_id"]);
      const admin = await auth.getUser();

      if (admin.role == "ADMIN") {
        const data_emp = await Database.select(
          "account_id",
          "first_name",
          "last_name",
          "email",
          "telephone",
          "hn_number",
          "role",
          "prefix",
          "profile_picture"
        )
          .from("accounts")
          .where({ account_id: account_id, is_active: 1 })
          .first();

        if (data_emp.role == "USER") {
          return response
            .status(204)
            .send("this account is not staff or admin");
        }

        const service_emp = await Database.select(
          "servicetypes.type_id AS type_id",
          "servicetypes.type_name AS type_name"
        )
          .from("work_times")
          .innerJoin(
            "servicetypes",
            "work_times.type_id",
            "servicetypes.type_id"
          )
          .where({
            "work_times.doctor_id": account_id,
            "servicetypes.availability": "AVAILABLE",
            "work_times.availability": "AVAILABLE",
          })
          .groupBy("type_id");

        return response.json({
          account_id: data_emp.account_id,
          prefix: data_emp.prefix,
          first_name: data_emp.first_name,
          last_name: data_emp.last_name,
          email: data_emp.email,
          telephone: data_emp.telephone,
          hn_number: data_emp.hn_number,
          role: data_emp.role,
          service_type: service_emp,
          profile_picture: data_emp.profile_picture,
        });
      } else if (admin.role == "STAFF") {
        if (account_id != admin.account_id) {
          return response.status(403).send();
        }

        const data_emp = await Database.select(
          "account_id",
          "first_name",
          "last_name",
          "role",
          "email",
          "telephone",
          "hn_number",
          "prefix",
          "profile_picture"
        )
          .from("accounts")
          .where({ account_id: admin.account_id, is_active: 1 })
          .first();

        if (data_emp.role == "USER") {
          return response
            .status(204)
            .send("this account is not staff or admin");
        }

        const service_emp = await Database.select(
          "servicetypes.type_id AS type_id",
          "servicetypes.type_name AS type_name"
        )
          .from("work_times")
          .innerJoin(
            "servicetypes",
            "work_times.type_id",
            "servicetypes.type_id"
          )
          .where({
            "work_times.doctor_id": admin.account_id,
            "servicetypes.availability": "AVAILABLE",
            "work_times.availability": "AVAILABLE",
          })
          .groupBy("type_id");

        return response.json({
          account_id: data_emp.account_id,
          prefix: data_emp.prefix,
          first_name: data_emp.first_name,
          last_name: data_emp.last_name,
          role: data_emp.role,
          email: data_emp.email,
          telephone: data_emp.telephone,
          hn_number: data_emp.hn_number,
          service_type: service_emp,
          profile_picture: data_emp.profile_picture,
        });
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      response.status(500).send(error);
    }
  }

  async editEmployeeSave({ request, response, auth }) {
    try {
      const admin = await auth.getUser();
      if (admin.role == "ADMIN") {
        const {
          account_id,
          first_name,
          last_name,
          email,
          telephone,
          hn_number,
          role,
          prefix,
          profile_picture,
        } = request.only([
          "account_id",
          "first_name",
          "last_name",
          "email",
          "telephone",
          "hn_number",
          "role",
          "prefix",
          "profile_picture",
        ]);

        //Check admin in system
        const count_admin = await Database.select(
          Database.raw("COUNT(*) AS number_of_admin")
        )
          .from("accounts")
          .whereRaw(`role = 'ADMIN' AND email != 'admin@mail.kmutt.ac.th'`);

        const oldAccount = await Account.find(account_id);

        if (
          count_admin[0].number_of_admin <= 1 &&
          role != "ADMIN" &&
          role != oldAccount.role
        ) {
          return response.status(204).send("HCARE must have atleast one admin");
        }

        //Check HNnumber
        const checkHn = await CheckHn.checkHnInSystem(hn_number);
        const accountInSys = await Account.find(account_id);
        //console.log(accountInSys);
        if (checkHn == false && accountInSys.hn_number != hn_number) {
          return response.status(203).send("รหัสพนักงานซ้ำกับที่มีในระบบ");
        }

        const old_data = await Account.query()
          .where({
            account_id: account_id,
            is_active: 1,
          })
          .update({
            first_name: first_name,
            last_name: last_name,
            email: email,
            telephone: telephone,
            hn_number: hn_number,
            role: role,
            prefix: prefix,
            edit_id: admin.account_id,
            profile_picture: profile_picture,
          });
        return response.status(201).send("updated success");
      } else if (admin.role == "STAFF") {
        const {
          account_id,
          first_name,
          last_name,
          email,
          telephone,
          hn_number,
          prefix,
          profile_picture,
        } = request.only([
          "account_id",
          "first_name",
          "last_name",
          "email",
          "telephone",
          "hn_number",
          "prefix",
          "profile_picture",
        ]);

        if (admin.account_id != account_id) {
          return response.status(403).send();
        }

        //Check HNnumber
        const checkHn = await CheckHn.checkHnInSystem(hn_number);
        const accountInSys = await Account.find(account_id);
        //console.log(accountInSys);
        if (checkHn == false && accountInSys.hn_number != hn_number) {
          return response.status(203).send("รหัสพนักงานซ้ำกับที่มีในระบบ");
        }

        const old_data = await Account.query()
          .where({
            account_id: account_id,
            is_active: 1,
          })
          .update({
            first_name: first_name,
            last_name: last_name,
            email: email,
            telephone: telephone,
            hn_number: hn_number,
            prefix: prefix,
            edit_id: admin.account_id,
            profile_picture: profile_picture,
          });
        return response.status(201).send("updated success");
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(500).send(error);
    }
  }

  async getEmployees({ response, auth }) {
    try {
      const admin = await auth.getUser();
      const role_admin = CheckRole.checkAdmin(admin.role, response);
      if (role_admin) {
        const all_emp = await Database.raw(`SELECT account_id, hn_number AS 'รหัสพนักงาน', prefix, name AS 'ชื่อนามสกุล', email AS 'อีเมล', telephone, GROUP_CONCAT(service) AS 'บริการ' , role FROM(
					SELECT DISTINCT accounts.account_id AS account_id , 
          accounts.hn_number as hn_number, 
          accounts.prefix,
					CONCAT(accounts.first_name,' ',accounts.last_name) AS name,
					accounts.email AS email, accounts.telephone AS telephone,
				  servicetypes.type_name AS service, accounts.role AS role
					FROM accounts LEFT JOIN work_times
					ON accounts.account_id = work_times.doctor_id
					LEFT JOIN servicetypes ON servicetypes.type_id = work_times.type_id
					WHERE accounts.is_active = 1 AND accounts.role IN ('STAFF','ADMIN') AND accounts.account_id != '1'
        ) AS SUB GROUP BY account_id;`);

        if (all_emp[0].length > 0) {
          all_emp[0].forEach((element) => {
            element.ชื่อนามสกุล =
              element.prefix == null
                ? element.ชื่อนามสกุล
                : element.prefix + " " + element.ชื่อนามสกุล;
          });
        }
        return response.status(200).send(all_emp[0]);
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async DeleteEmployee({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const { account_id, email } = request.only(["account_id", "email"]);
      if (account.role == "ADMIN") {
        const userAccount = await Database.select("*")
          .from("accounts")
          .where({ account_id: account_id, email: email })
          .first();

        if (
          userAccount &&
          (userAccount.role == "ADMIN" || userAccount.role == "STAFF")
        ) {
          const checkAdmin = await Account.query()
            .where({ role: "ADMIN" })
            .getCount();
          if (userAccount.role == "ADMIN" && checkAdmin < 1) {
            return response
              .status(203)
              .send("จำเป็นต้องมี ADMIN ดูแลระบบอย่างน้อย 1 คน");
          } else {
            await Booking.query()
              .whereRaw(
                `account_id_from_user = '${
                  userAccount.account_id
                }' AND date >= '${new Date().toISOString().slice(0, 10)}'`
              )
              .update({
                status: null,
                comment_from_user: null,
                comment_from_staff: null,
                token_booking_confirm: null,
                link_meeting: null,
                account_id_from_user: null,
                account_id_from_staff: null,
              });

            await Database.raw(
              `UPDATE histories INNER JOIN bookings ON histories.booking_id = bookings.booking_id INNER JOIN work_times ON bookings.working_id = work_times.working_id SET histories.is_active = '0' WHERE work_times.doctor_id = '${
                userAccount.account_id
              }' AND histories.date >= '${new Date()
                .toISOString()
                .slice(0, 10)}'`
            );

            await History.query()
              .where({ account_id: userAccount.account_id })
              .delete();

            await WorkTime.query()
              .where({ doctor_id: userAccount.account_id })
              .delete();

            await Account.query()
              .where({
                account_id: userAccount.account_id,
                email: userAccount.email,
              })
              .delete();

            return response.status(200).send("delete account successfully");
          }
        } else if (userAccount && userAccount.role == "USER") {
          await Booking.query()
            .whereRaw(
              `account_id_from_user = '${
                userAccount.account_id
              }' AND date >= '${new Date().toISOString().slice(0, 10)}'`
            )
            .update({
              status: null,
              comment_from_user: null,
              comment_from_staff: null,
              token_booking_confirm: null,
              link_meeting: null,
              account_id_from_user: null,
              account_id_from_staff: null,
            });

          await History.query()
            .where({ account_id: userAccount.account_id })
            .delete();

          await Account.query()
            .where({
              account_id: userAccount.account_id,
              email: userAccount.email,
            })
            .delete();
        } else {
          return response.status(203).send("ไม่มีบัญชีนี้ในระบบ");
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }
}

module.exports = ManageEmployeeController;
