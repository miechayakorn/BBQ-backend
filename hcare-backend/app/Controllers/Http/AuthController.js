"use strict";
const Account = use("App/Models/Account");
const Hash = use("Hash");
const Database = use("Database");
const Mail = use("Mail");
const Env = use("Env");
const CryptoJS = require("crypto-js");
const CheckMail = use("App/Service/CheckMail");

class AuthController {
  async authenticate({ request, response }) {
    try {
      const { email } = request.only(["email"]);

      const account = await Database.select("*")
        .from("accounts")
        .where({ email: email, is_active: 1 })
        .first();

      // console.log("------------------ Account ---------------------");

      if (account) {
        if (account.role == "USER") {
          const digits = "0123456789";
          var otp = "";
          for (let i = 0; i < 6; i++) {
            otp = otp + digits[Math.floor(Math.random() * 10)];
          }

          // console.log("--------------- OTP --------------------");

          const dataSendEmail = {
            account: account.hn_number,
            email: account.email,
            otp,
            url: Env.get("VUE_APP_FONTEND_URL"),
          };

          // console.log("--------------- DATA SEND MAIL --------------------");

          const mail = await Mail.send("login", dataSendEmail, (message) => {
            message
              .from(Env.get("MAIL_USERNAME"))
              .to(account.email)
              .subject("Login to HCARE");
          });

          // console.log("--------------- Mail --------------------");

          await Account.query()
            .where("account_id", account.account_id)
            .update({
              password: await Hash.make(dataSendEmail.otp),
            });

          const accountLogin = await Database.select("*")
            .from("accounts")
            .where({ email: email, is_active: 1 })
            .first();
          // console.log("--------------- New Password ------------------");

          if (account.password != accountLogin.password) {
            return "send mail for login successful";
          } else {
            return "password don't have changed";
          }
        } else {
          return response
            .status(403)
            .send("You don't have permission to user login");
        }
      } else {
        return response.status(401).send();
      }
    } catch (error) {
      //console.log(error);
      return response
        .status(401)
        .json({ message: "You are not registered!", error });
    }
  }

  async confirmauthenticate({ request, response, auth }) {
    const data = request.only(["email", "password"]);

    try {
      if (await auth.attempt(data.email, data.password)) {
        let account = await Account.findBy("email", data.email);
        let token = await auth.generate(account);

        let dataResp = {
          account_id: account.account_id,
          first_name: account.first_name,
          last_name: account.last_name,
          role: account.role,
          type: token.type,
          token: token.token,
          refreshToken: token.refreshToken,
          profile_picture: account.profile_picture,
        };

        return response.json(dataResp);
      }
    } catch (error) {
      return response
        .status(401)
        .json({ message: "You are not registered!", error });
    }
  }

  async oauth({ request, response, auth }) {
    try {
      const { hash } = request.only(["hash"]);
      const key = Env.get("SECRET_KEY");
      const email = CryptoJS.AES.decrypt(hash, key).toString(CryptoJS.enc.Utf8);
      const account = await Account.findBy("email", email);
      const checkMail = await CheckMail.checkMailKMUTT(email);

      if (checkMail == false) {
        return response.status(203).send("please use @mail.kmutt.ac.th only");
      }

      if (account) {
        let token = await auth.generate(account);
        let dataResp = {
          account_id: account.account_id,
          first_name: account.first_name,
          last_name: account.last_name,
          role: account.role,
          type: token.type,
          token: token.token,
          refreshToken: token.refreshToken,
          profile_picture: account.profile_picture,
        };
        return response.json(dataResp);
      } else {
        return response.status(202).send("no registered");
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async myprofile({ response, auth }) {
    try {
      const user = await auth.getUser();
      return response.json({ user: user });
    } catch (error) {
      console.log(error);
    }
  }

  /*async logout({ request, response, auth }) {
    try {
      console.log("sdasdsaaaaaaaaaaaaaaaaaaaa");
      const user = await auth.getUser();
      await auth.authenticator("jwt").revokeTokens();
      return response.status(201).send("success");
    } catch (error) {
      return response.status(500).send(error);
    }
  }*/
}

module.exports = AuthController;
