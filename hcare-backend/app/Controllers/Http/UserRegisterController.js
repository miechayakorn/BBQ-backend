"use strict";
const Database = use("Database");
const Account = use("App/Models/Account");
const Token = use("App/Models/Token");
const Mail = use("Mail");
const Hash = use("Hash");
const Env = use("Env");
const RegisterRules = use("App/Validators/Register");
const { validateAll } = use("Validator");
const CheckHn = use("App/Service/CheckHN");

class UserRegisterController {
  // create user and sendmail to confirm
  async createUser({ request, response }) {
    try {
      const data = request.only([
        "hn_number",
        "email",
        "telephone",
        "first_name",
        "last_name",
      ]);
      await validateAll(data, RegisterRules);

      const checkHn = await CheckHn.checkHnInSystem(data.hn_number);
      if (checkHn == false) {
        return response
          .status(203)
          .send("รหัสนักศึกษาหรือรหัสพนักงานซ้ำกับที่มีในระบบ");
      }

      const accountUser = await Account.create({
        hn_number: data.hn_number,
        first_name: data.first_name,
        last_name: data.last_name,
        role: "USER",
        email: data.email,
        telephone: data.telephone,
        verify: "SUCCESS",
      });

      if (accountUser) {
        const token = `${Date.now()}${accountUser.$attributes.hn_number}`;
        const tokenHash = await Hash.make(token);
        await Token.create({
          account_id: accountUser.$attributes.account_id,
          token: tokenHash,
          type: "REGISTER",
          is_revoked: "1",
        });
        // const dataForSendEmail = {
        //   account: await Database.table("accounts")
        //     .where("account_id", accountUser.$attributes.account_id)
        //     .first(),
        //   tokenHash,
        //   url: Env.get("VUE_APP_FONTEND_URL"),
        // };

        // const sendMail = await Mail.send(
        //   "activateaccount",
        //   dataForSendEmail,
        //   (message) => {
        //     message
        //       .from(Env.get("MAIL_USERNAME"))
        //       .to(dataForSendEmail.account.email)
        //       .subject("Activate Register From Health Care");
        //   }
        // );

        // if (sendMail) {
        //   const createTokenDB = await Token.create({
        //     account_id: dataForSendEmail.account.account_id,
        //     token: tokenHash,
        //     type: "REGISTER",
        //   });
        //   return "sendmail success";
        // } else {
        //   return response.status(500).send("cannot sendmail");
        // }
        return response.status(200).send("Create User Successfully");
      }
    } catch (err) {
      console.log(err);
      return response.status(err.status).send(err);
    }
  }

  // confirm after click in email
  async confirmRegister({ request, response }) {
    const query = request.get();
    try {
      if (query.token) {
        const accountConfirm = await Token.findBy("token", query.token);
        if (accountConfirm) {
          await Account.query()
            .where("account_id", accountConfirm.account_id)
            .update({ verify: "SUCCESS" });
          await Token.query()
            .where({
              token: query.token,
              account_id: accountConfirm.account_id,
              type: "REGISTER",
            })
            .update({ is_revoked: 1 });
          return response.json({
            message: "Registration confirmation successful",
          });
        } else {
          return response.status(304).json({
            message: "This token is not available",
          });
        }
      } else {
        return response.status(500).json({
          message: "Token not exist",
        });
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }
}

module.exports = UserRegisterController;
