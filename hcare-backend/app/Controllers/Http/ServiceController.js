"use strict";

const ServiceType = use("App/Models/ServiceType");
const WorkTime = use("App/Models/WorkTime");
const Database = use("Database");
const History = use("App/Models/History");
const moment = require("moment");
moment.locale();

class ServiceController {
  async getService({ response, auth }) {
    try {
      const admin = await auth.getUser();
      if (admin.role == "STAFF") {
        const service_staff1 = await Database.raw(
          `SELECT GROUP_CONCAT( DISTINCT type_id) AS type_id FROM work_times WHERE doctor_id = ${admin.account_id}`
        );
        let service_staff = service_staff1[0][0];
        const staff_list = await Database.select(
          "account_id",
          "prefix",
          "first_name",
          "last_name",
          "role",
          "type_id",
          "profile_picture"
        )
          .from("accounts")
          .innerJoin(
            "work_times",
            "accounts.account_id",
            "work_times.doctor_id"
          )
          .whereRaw(
            `type_id IN (${service_staff.type_id}) AND accounts.is_active = 1`
          )
          .groupByRaw(
            `account_id, prefix, first_name, last_name, role, type_id`
          );

        const service_db = await Database.select(
          "type_id",
          "type_name",
          "locations.location_id",
          "location_name",
          "availability"
        )
          .from("servicetypes")
          .innerJoin(
            "locations",
            "servicetypes.location_id",
            "locations.location_id"
          )
          .whereRaw(`type_id IN (${service_staff.type_id})`);

        let service = service_db.map((subService) => {
          return {
            type_id: subService.type_id,
            type_name: subService.type_name,
            location_id: subService.location_id,
            location_name: subService.location_name,
            availability: subService.availability,
            status: subService.availability == "AVAILABLE" ? true : false,
          };
        });

        let data_res = [];
        if (service.length > 0) {
          data_res = service;
          for (let i = 0; i < data_res.length; i++) {
            let data = [];
            for (let j = 0; j < staff_list.length; j++) {
              if (data_res[i].type_id == staff_list[j].type_id) {
                data.push(staff_list[j]);
              }
            }
            Object.assign(data_res[i], { staff_list: data });
          }
          return data_res;
        }
        return response.status(204).send();
      } else if (admin.role == "ADMIN") {
        const staff_list = await Database.select(
          "account_id",
          "prefix",
          "first_name",
          "last_name",
          "role",
          "type_id",
          "profile_picture"
        )
          .from("accounts")
          .innerJoin(
            "work_times",
            "accounts.account_id",
            "work_times.doctor_id"
          )
          .groupByRaw(
            `account_id, prefix, first_name, last_name, role, type_id`
          );

        const service_db = await Database.select(
          "type_id",
          "type_name",
          "locations.location_id",
          "location_name",
          "availability"
        )
          .from("servicetypes")
          .innerJoin(
            "locations",
            "servicetypes.location_id",
            "locations.location_id"
          );

        let service = service_db.map((subService) => {
          return {
            type_id: subService.type_id,
            type_name: subService.type_name,
            location_id: subService.location_id,
            location_name: subService.location_name,
            availability: subService.availability,
            status: subService.availability == "AVAILABLE" ? true : false,
          };
        });

        let data_res = [];
        if (service.length > 0) {
          data_res = service;
          for (let i = 0; i < data_res.length; i++) {
            let data = [];
            for (let j = 0; j < staff_list.length; j++) {
              if (data_res[i].type_id == staff_list[j].type_id) {
                data.push(staff_list[j]);
              }
            }
            Object.assign(data_res[i], { staff_list: data });
          }
          return data_res;
        }
        return response.status(204).send();
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async createService({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      if (account.role != "ADMIN") {
        return response.status(403).send();
      } else {
        const { service_name, location_id } = request.only([
          "service_name",
          "location_id",
        ]);
        const serviceDb = await Database.table("servicetypes").where({
          location_id: location_id,
          type_name: service_name.trim(),
        });
        if (serviceDb.length <= 0) {
          await ServiceType.create({
            type_name: service_name,
            location_id: location_id,
            availability: "AVAILABLE",
            add_id: account.account_id,
          });
          return response.status(201).send("success");
        } else {
          return response.status(200).send("duplicate service");
        }
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async getServiceForAdd({ response, auth }) {
    try {
      const account = await auth.getUser();
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const condition =
          account.role == "STAFF"
            ? `AND doctor_id = '${account.account_id}'`
            : "";
        const location = await Database.select("location_id", "location_name")
          .from("locations")
          .where({ is_active: 1 });
        const services = await Database.select(
          "locations.location_id AS location_id",
          "locations.location_name AS location_name",
          "servicetypes.type_id",
          "servicetypes.type_name"
        )
          .from("servicetypes")
          .innerJoin(
            "locations",
            "servicetypes.location_id",
            "locations.location_id"
          )
          .leftJoin("work_times", "servicetypes.type_id", "work_times.type_id")
          .whereRaw(
            `servicetypes.availability = 'AVAILABLE' AND is_active = '1' ${condition} `
          )
          .groupByRaw(
            "locations.location_id, locations.location_name, servicetypes.type_id, servicetypes.type_name"
          );

        if (location.length > 0) {
          let arr_result = location;
          for (let i = 0; i < arr_result.length; i++) {
            let data = [];
            for (let j = 0; j < services.length; j++) {
              if (arr_result[i].location_id == services[j].location_id) {
                data.push({
                  type_id: services[j].type_id,
                  type_name: services[j].type_name,
                });
              }
            }
            Object.assign(arr_result[i], { service: data });
          }
          return response.send(arr_result);
        } else {
          return response.status(204).send();
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  async getEmployeesForAdd({ response, auth }) {
    try {
      const account = await auth.getUser();
      if (account.role == "ADMIN") {
        const staff = await Database.select(
          "account_id",
          "prefix",
          "first_name",
          "last_name"
        )
          .from("accounts")
          .whereRaw(
            `is_active = 1 AND role IN ('STAFF','ADMIN') AND accounts.account_id != '1'`
          );
        return staff;
      } else if (account.role == "STAFF") {
        const staff = await Database.select(
          "account_id",
          "prefix",
          "first_name",
          "last_name"
        )
          .from("accounts")
          .whereRaw(
            `is_active = 1 AND role IN ('STAFF','ADMIN') AND accounts.account_id != '1' AND account_id = ${account.account_id}`
          );
        return staff;
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      response.status(500).send(error);
    }
  }

  async getServiceForEdit({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const { location_id } = request.only(["location_id"]);
      if (account.role == "ADMIN") {
        const serviceData = await Database.select(
          "type_id",
          "type_name",
          "location_id"
        )
          .from("servicetypes")
          .where({ location_id: location_id, availability: "AVAILABLE" });
        return serviceData;
      } else if (account.role == "STAFF") {
        const serviceData = await Database.select(
          "servicetypes.type_id",
          "type_name",
          "location_id",
          "doctor_id AS account_id"
        )
          .from("servicetypes")
          .innerJoin("work_times", "servicetypes.type_id", "work_times.type_id")
          .where({
            location_id: location_id,
            "servicetypes.availability": "AVAILABLE",
            doctor_id: account.account_id,
          })
          .groupByRaw("servicetypes.type_id, type_name, location_id");
        if (serviceData.length > 0) {
          return serviceData;
        } else {
          return response.status(204).send();
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async getEmployeesForEditService({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const { type_id } = request.only(["type_id"]);
      if (account.role == "ADMIN") {
        const emp = await Database.select("type_id", "account_id", "prefix")
          .select(Database.raw(" CONCAT(first_name,' ',last_name) AS name"))
          .from("work_times")
          .innerJoin("accounts", "work_times.doctor_id", "accounts.account_id")
          .whereRaw(`work_times.type_id = ${type_id}`)
          .groupByRaw(`account_id`);
        if (emp.length > 0) {
          return emp;
        } else {
          return response.status(204).send();
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async updateServiceStatus({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const { type_id, status, announcement } = request.only([
        "type_id",
        "status",
        "announcement",
      ]);
      if (account.role == "ADMIN" || account.role == "STAFF") {
        let availability_db = status == true ? "AVAILABLE" : "UNAVAILABLE";
        const st = await ServiceType.find(type_id);
        const wt = await WorkTime.findBy("type_id", type_id);
        if (st.length > 0 && wt.length > 0) {
          await Database.table("servicetypes")
            .innerJoin(
              "work_times",
              "servicetypes.type_id",
              "work_times.type_id"
            )
            .innerJoin(
              "bookings",
              "work_times.working_id",
              "bookings.working_id"
            )
            .whereRaw(
              `servicetypes.type_id = '${type_id}' AND work_times.type_id = '${type_id}' AND bookings.date >= '${moment().format(
                "YYYY-MM-DD"
              )}'`
            )
            .update({
              "servicetypes.availability": availability_db,
              "servicetypes.edit_id": account.account_id,
              "servicetypes.updated_at": moment().format("YYYY-MM-DD HH:mm:ss"),
              "servicetypes.announcement": announcement,
              "work_times.availability": availability_db,
              "work_times.edit_id": account.account_id,
              "work_times.updated_at": moment().format("YYYY-MM-DD HH:mm:ss"),
              "bookings.availability": availability_db,
              "bookings.edit_id": account.account_id,
            });
          return response.status(201).send("success");
        } else if (await ServiceType.find(type_id)) {
          await ServiceType.query().where({ type_id: type_id }).update({
            availability: availability_db,
            announcement: announcement,
            edit_id: account.account_id,
          });
          return response.status(201).send("success");
        } else {
          return response.status(200).send("Don't have this service");
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  async deleteService({ response, auth, params }) {
    try {
      const account = await auth.getUser();
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const { type_id } = params;
        const servicetype = await ServiceType.find(type_id);
        if (servicetype) {
          let type = servicetype.toJSON();
          await History.query()
            .whereRaw(
              `type_name = '${type.type_name}' AND date >= '${moment().format(
                "YYYY-MM-DD"
              )}'`
            )
            .update({ is_active: 0 });
          await servicetype.delete();

          return response.status(200).send("delete service success");
        } else {
          return response.status(204).send();
        }
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(500).send(error);
    }
  }

  async announcementAllService({ response }) {
    try {
      const announcement = await Database.select("*")
        .from("servicetypes")
        .where({ availability: "UNAVAILABLE" });
      let dataRes = await announcement.map((announce) => {
        let data = {
          type_id: announce.type_id,
          type_name: announce.type_name,
          availability: announce.availability,
          announcement: announce.announcement,
          date: announce.updated_at.toISOString().slice(0, 10),
        };
        return data;
      });
      return response.status(200).send(dataRes);
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }
}

module.exports = ServiceController;
