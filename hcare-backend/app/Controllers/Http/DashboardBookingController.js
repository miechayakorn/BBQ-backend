"use strict";
const Booking = use("App/Models/Booking");
const Account = use("App/Models/Account");
const Database = use("Database");
const Mail = use("Mail");
const Env = use("Env");
const DateFormat = use("App/Service/DateService");
const History = use("App/Models/History");

class DashboardBookingController {
  //แสดงตารางนัดหมายตามประเภทและเวลาที่ระบุ
  async showBookingForHCARE({ response, params, auth }) {
    try {
      const accountHC = await auth.getUser();
      if (accountHC.role == "STAFF") {
        let userBooking = await Database.select(
          "bookings.booking_id AS booking_id",
          "accounts.account_id AS account_id",
          "hn_number AS HNnumber ",
          "first_name AS ชื่อ",
          "last_name AS นามสกุล",
          "time_in AS เวลานัด",
          "type_id",
          "date",
          "email",
          "telephone",
          "comment_from_user as symptom",
          "link_meeting",
          "comment_from_staff"
        )
          .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as date'))
          .from("bookings")
          .innerJoin(
            "accounts",
            "bookings.account_id_from_user",
            "accounts.account_id"
          )
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .where({
            status: "CONFIRM SUCCESS",
            type_id: params.type,
            date: params.date,
            doctor_id: accountHC.account_id,
          });

        for (let index = 0; index < userBooking.length; index++) {
          userBooking[index].date = await DateFormat.ChangeDateFormat(
            userBooking[index].date
          );
        }

        if (userBooking.length > 0) {
          return userBooking;
        } else {
          return response.status(204).send();
        }
      } else if (accountHC.role == "ADMIN") {
        let userBooking = await Database.select(
          "booking_id",
          "account_id",
          "hn_number AS HNnumber ",
          "first_name AS ชื่อ",
          "last_name AS นามสกุล",
          "time_in AS เวลานัด",
          "type_id",
          "date",
          "email",
          "telephone",
          "comment_from_user as symptom",
          "link_meeting",
          "comment_from_staff"
        )
          .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as date'))
          .from("bookings")
          .innerJoin(
            "accounts",
            "bookings.account_id_from_user",
            "accounts.account_id"
          )
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .where({
            status: "CONFIRM SUCCESS",
            type_id: params.type,
            date: params.date,
          });

        for (let index = 0; index < userBooking.length; index++) {
          userBooking[index].date = await DateFormat.ChangeDateFormat(
            userBooking[index].date
          );
        }
        if (userBooking.length > 0) {
          return userBooking;
        } else {
          return response.status(204).send();
        }
      }
      return response.status(403).send();
    } catch (error) {
      //console.log(`Error: ${error}`);
      return response.status(500).send(error);
    }
  }

  /*เพิ่ม link และ note สำหรับ Health care*/
  async editPatientBooking({ request, response, auth }) {
    try {
      const accountHC = await auth.getUser();
      if (accountHC.role == "STAFF" || accountHC.role == "ADMIN") {
        const dataEditPatientBook = request.all(["booking_id", "link", "note"]);

        const booking = await Booking.find(dataEditPatientBook.booking_id);

        if (booking) {
          if (dataEditPatientBook.link && dataEditPatientBook.note) {
            await Booking.query()
              .where("booking_id", booking.booking_id)
              .update({
                link_meeting: dataEditPatientBook.link,
                comment_from_staff: dataEditPatientBook.note,
                edit_id: accountHC.account_id,
              });
            await History.query()
              .where({ booking_id: booking.booking_id, is_active: 1 })
              .update({ comment_from_staff: dataEditPatientBook.note });
          } else if (dataEditPatientBook.note) {
            await Booking.query()
              .where("booking_id", booking.booking_id)
              .update({
                link_meeting: null,
                comment_from_staff: dataEditPatientBook.note,
                edit_id: accountHC.account_id,
              });
            await History.query()
              .where({ booking_id: booking.booking_id, is_active: 1 })
              .update({ comment_from_staff: dataEditPatientBook.note });
          } else if (dataEditPatientBook.link) {
            await Booking.query()
              .where("booking_id", booking.booking_id)
              .update({
                link_meeting: dataEditPatientBook.link,
                comment_from_staff: null,
                edit_id: accountHC.account_id,
              });
          } else if (!dataEditPatientBook.link && !dataEditPatientBook.note) {
            await Booking.query()
              .where("booking_id", booking.booking_id)
              .update({
                link_meeting: null,
                comment_from_staff: null,
                edit_id: accountHC.account_id,
              });
            await History.query()
              .where({ booking_id: booking.booking_id, is_active: 1 })
              .update({ comment_from_staff: null });
          }

          let returnBooking = await Booking.find(booking.booking_id);
          return response.json({
            message: "booking update successful!",
            booking: returnBooking,
          });
        } else {
          return "Have no this booking";
        }
      }
      return response.status(403).send();
    } catch (error) {
      response.status(500).send(error);
    }
  }

  //ยกเลิกการจองนัดของผู้ป่วยผ่านหน้า Dashboard
  async cancelAppointment({ request, response }) {
    try {
      const dataCancel = await request.only(["booking_id"]);

      if (dataCancel.booking_id) {
        const booking = await Booking.find(dataCancel.booking_id);

        if (booking.status != null) {
          const findBooking = await Database.select(
            "bookings.booking_id AS booking_id",
            "servicetypes.type_name AS type_name",
            "bookings.time_in AS time_in",
            "bookings.time_out AS time_out",
            "bookings.date AS date",
            "bookings.status AS status"
          )
            .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as date'))
            .from("bookings")
            .innerJoin(
              "work_times",
              "bookings.working_id",
              "work_times.working_id"
            )
            .innerJoin(
              "servicetypes",
              "work_times.type_id",
              "servicetypes.type_id"
            )
            .where("bookings.booking_id", booking.booking_id)
            .first();

          findBooking.date = DateFormat.ChangeDateFormat(findBooking.date);
          findBooking.time_in = findBooking.time_in.substring(0, 5);
          findBooking.time_out = findBooking.time_out.substring(0, 5);

          const account = await Account.find(booking.account_id_from_user);

          const dataForSendEmail = {
            account: account,
            bookingSlot: findBooking,
            url: Env.get("VUE_APP_FONTEND_URL"),
            mail_subject: "Healthcare ได้ทำการยกเลิกนัดหมายของคุณ",
          };

          const subject =
            "Healthcare Cancel booking " +
            dataForSendEmail.bookingSlot.type_name.toString();

          await Mail.send("sendmailcancel", dataForSendEmail, (message) => {
            message
              .to(account.email)
              .from(Env.get("MAIL_USERNAME"))
              .subject(subject);
          });
          await Booking.query()
            .where("booking_id", dataCancel.booking_id)
            .update({
              status: null,
              comment_from_user: null,
              comment_from_staff: null,
              token_booking_confirm: null,
              link_meeting: null,
              account_id_from_user: null,
              account_id_from_staff: null,
            });
          const bookingUpdate = await Database.from("bookings").where(
            "booking_id",
            booking.booking_id
          );
          await History.query()
            .where({ booking_id: booking.booking_id, is_active: 1 })
            .update({ is_active: 0 });

          return response.json({
            message: "clear schedule successful",
            booking: bookingUpdate,
          });
        } else {
          return response
            .status(304)
            .json({ message: "Don't have booking in database" });
        }
      } else {
        return response
          .status(500)
          .json({ message: "Booking ID does not exist" });
      }
    } catch (error) {
      response.status(500).send(error);
    }
  }

  //จองตารางนัดหมายโดย Healthcare
  async submitBookingFromHealthcare({ request, response, auth }) {
    try {
      const accountHC = await auth.getUser();
      const { booking_id, email, symptom } = request.only([
        "booking_id",
        "email",
        "symptom",
      ]);
      if (accountHC.role == "STAFF" || accountHC.role == "ADMIN") {
        //find account from hn_number
        const userAccount = await Database.select(
          "account_id",
          "email",
          "first_name",
          "last_name"
        )
          .from("accounts")
          .where({ email: email, is_active: 1 })
          .first();

        // find booking slot from booking_id that get from request to find in DB
        const findBooking = await Database.select(
          "bookings.booking_id AS booking_id",
          "servicetypes.type_name AS type_name",
          "bookings.time_in AS time_in",
          "bookings.time_out AS time_out",
          "bookings.date AS date",
          "bookings.status AS status"
        )
          .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as date'))
          .from("bookings")
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .innerJoin(
            "servicetypes",
            "work_times.type_id",
            "servicetypes.type_id"
          )
          .where("bookings.booking_id", booking_id)
          .first();

        findBooking.date = DateFormat.ChangeDateFormat(findBooking.date);
        //console.log(findBooking);
        findBooking.time_in = findBooking.time_in.substring(0, 5);
        findBooking.time_out = findBooking.time_out.substring(0, 5);

        if (userAccount) {
          // check account not null

          if (!findBooking.status) {
            // check booking status available
            const dataForSendEmail = {
              account: userAccount,
              bookingSlot: findBooking,
              url: Env.get("VUE_APP_FONTEND_URL"),
            };

            const subject =
              "Boooking By Health Care  " +
              dataForSendEmail.bookingSlot.type_name.toString();

            await Mail.send("sendmailbooking", dataForSendEmail, (message) => {
              message
                .to(userAccount.email)
                .from(Env.get("MAIL_USERNAME"))
                .subject(subject);
            });

            await Booking.query().where({ booking_id: booking_id }).update({
              account_id_from_user: dataForSendEmail.account.account_id,
              status: "CONFIRM SUCCESS",
              comment_from_user: symptom,
              account_id_from_staff: accountHC.account_id,
            });

            let booking_details = await Database.select(
              "locations.location_name",
              "servicetypes.type_name",
              "bookings.booking_id",
              "bookings.date",
              "bookings.time_in",
              "bookings.time_out",
              "bookings.comment_from_user",
              "bookings.comment_from_staff",
              "bookings.account_id_from_user",
              "accounts.prefix",
              Database.raw(
                "CONCAT(accounts.first_name,' ',accounts.last_name) as doctor_name"
              )
            )
              .from("bookings")
              .innerJoin(
                "work_times",
                "bookings.working_id",
                "work_times.working_id"
              )
              .innerJoin(
                "servicetypes",
                "work_times.type_id",
                "servicetypes.type_id"
              )
              .innerJoin(
                "locations",
                "servicetypes.location_id",
                "locations.location_id"
              )
              .innerJoin(
                "accounts",
                "work_times.doctor_id",
                "accounts.account_id"
              )
              .where({ booking_id: booking_id })
              .first();

            await History.create({
              location: booking_details.location_name,
              type_name: booking_details.type_name,
              booking_id: booking_details.booking_id,
              date: booking_details.date,
              time_in: booking_details.time_in,
              time_out: booking_details.time_out,
              comment_from_user: booking_details.comment_from_user,
              comment_from_staff: booking_details.comment_from_staff,
              account_id: booking_details.account_id_from_user,
              prefix_doctor: booking_details.prefix,
              doctor_name: booking_details.doctor_name,
              is_active: 1,
            });

            return response.status(200).send("send mail success");
          }
          return response.status(400).send("This booking unavailable");
        }
      }
      return response.status(403).send();
    } catch (error) {
      console.log(error);
      return response.status(500).send(error);
    }
  }
}

module.exports = DashboardBookingController;
