"use strict";
const { validate } = use("Validator");
const Database = use("Database");
const moment = require("moment");
moment.locale();

class ServiceScheduleController {
  async getServiceSchedule({ request, response }) {
    try {
      const { location_id } = request.only(["location_id"]);
      const service = await Database.select("*")
        .from("work_times")
        .innerJoin("servicetypes", "work_times.type_id", "servicetypes.type_id")
        .innerJoin(
          "locations",
          "servicetypes.location_id",
          "locations.location_id"
        )
        .where({
          "locations.location_id": location_id,
          "work_times.availability": "AVAILABLE",
          "servicetypes.availability": "AVAILABLE",
          "locations.is_active": 1,
        });
      service.forEach((element) => {
        switch (element.day) {
          case "SUNDAY":
            element.day = 1;
            break;
          case "MONDAY":
            element.day = 2;
            break;
          case "TUESDAY":
            element.day = 3;
            break;
          case "WEDNESDAY":
            element.day = 4;
            break;
          case "THURSDAY":
            element.day = 5;
            break;
          case "FRIDAY":
            element.day = 6;
            break;
          case "SATURDAY":
            element.day = 7;
            break;
          default:
            element.day = element.day;
            break;
        }
      });

      let serviceWeek = [];
      let colors = [
        "#99A3FF",
        "#895C1A",
        "#BAFFBC",
        "#C891FF",
        "#FF8B8B",
        "#00C6D2",
        "#A51954",
        "#3B65B8",
        "#FF3EC9",
        "#FF6928",
        "#3431BD",
        "#55E0FF",
        "#FFD66B",
        "#47D044",
        "#8075FF",
        "#FF3636",
        "#EE6262",
        "#978EFF",
        "#7ED17D",
        "#FFDA7B",
        "#9FEEFF",
        "#706EE1",
        "#FF8E5D",
        "#F48FD8",
      ];

      for (let i = 0; i < service.length; i++) {
        let findindex = serviceWeek.findIndex(
          (data) => data.type_id === service[i].type_id
        );
        if (findindex == -1) {
          let data = {
            type_id: service[i].type_id,
            dot: { backgroundColor: colors.pop() },
            dates: { weekdays: [service[i].day] },
          };
          serviceWeek.push(data);
        } else {
          serviceWeek[findindex].dates.weekdays.push(service[i].day);
        }
      }

      serviceWeek.forEach((element) => {
        delete element.type_id;
      });

      if (serviceWeek.length <= 0) {
        return response.status(204).send();
      } else {
        return response.status(200).send(serviceWeek);
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error.message);
    }
  }

  async getServiceScheduleDetail({ request, response }) {
    try {
      const { date, location_id } = request.only(["date", "location_id"]);
      const validation = await validate(
        { date, location_id },
        { date: "required", location_id: "required" }
      );
      if (validation.fails()) {
        return response.send(validation.messages());
      }

      const day = moment(date).format("dddd").toUpperCase();
      const work_times = await Database.select(
        "working_id",
        "start_time",
        "end_time",
        "day",
        "servicetypes.type_id",
        "type_name",
        "prefix",
        "first_name",
        "last_name",
        "profile_picture"
      )
        .from("work_times")
        .innerJoin("servicetypes", "work_times.type_id", "servicetypes.type_id")
        .innerJoin("accounts", "accounts.account_id", "work_times.doctor_id")
        .whereRaw(
          `location_id = '${location_id}' AND day = '${day}' AND work_times.availability = 'AVAILABLE' AND servicetypes.availability='AVAILABLE'`
        )
        .orderByRaw("type_id");

      if (work_times.length <= 0) {
        return response.status(204).send();
      } else {
        return response.status(200).send(work_times);
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error.message);
    }
  }
}

module.exports = ServiceScheduleController;
