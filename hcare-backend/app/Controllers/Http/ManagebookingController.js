"use strict";
const Database = use("Database");
const DateService = use("App/Service/DateService");
const CreateSlotRules = use("App/Validators/CreateSlotBooking");
const { validateAll } = use("Validator");
const Booking = use("App/Models/Booking");
const moment = require("moment");
moment.locale();

class ManagebookingController {
  //รับ type และวันที่หาช่วงเวลาทำงานเพื่อนำไปเพิ่ม slot
  async CheckTimeslot({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      const { type_id, date, doctor_id } = request.only([
        "type_id",
        "date",
        "doctor_id",
      ]);
      let dateNewFormat = new Date(date);
      let day = DateService.dateToDay(dateNewFormat.getDay());

      const slot = await Database.table("work_times")
        .select("*")
        .where({
          type_id: type_id,
          day: day,
          availability: "AVAILABLE",
          doctor_id: doctor_id,
        })
        .first();

      //return 204 ในกรณี type_id และ date ไม่มีใน work_times
      if (slot == undefined) {
        return response.status(204).send();
      }

      const minsToAdd = slot.time_slot;
      const start_time = slot.start_time;
      const end_time = slot.end_time;
      let time_count = start_time;
      let start = new Date(new Date(`1970/01/01"  ${start_time}`)).getTime();
      let end = new Date(new Date(`1970/01/01"  ${end_time}`)).getTime();
      let round = (end - start) / 60000 / minsToAdd;

      //key = working_id + date + time_in
      const con = `${slot.working_id}${date.slice(0, 10).replace(/-/g, "")}%`;
      const timeslot_db = await Database.from("bookings")
        .where("key_slot", "LIKE", con)
        .orderByRaw("time_in, date");

      const timeslot = timeslot_db.map(function (timeslot_db) {
        return timeslot_db.time_in;
      });

      let timeArray = [];

      for (let i = 0; i < round; i++) {
        timeArray.push({
          slot: time_count,
          status:
            timeslot.indexOf(time_count) == -1 ? "available" : "unavailable",
        });
        time_count = new Date(
          new Date("1970/01/01 " + time_count).getTime() + minsToAdd * 60000
        ).toLocaleTimeString("en-UK", {
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit",
          hour12: false,
        });
      }

      let arr_time_slot = timeArray.map(function (timeArray) {
        if (timeArray.status == "available") {
          Object.assign(timeArray, { toggle: false });
        } else if (timeArray.status == "unavailable") {
          Object.assign(timeArray, { toggle: null });
        }
        return timeArray;
      });

      return response.json({
        type_id: type_id,
        date: date,
        working_id: slot.working_id,
        doctor_id: slot.doctor_id,
        date_use: DateService.ChangeDateFormat(
          moment(date).format("dddd D MMMM YYYY")
        ),
        timeArray: arr_time_slot,
      });
    } catch (error) {
      return response.status(error.status).send(error);
    }
  }

  // บันทึกการสร้าง slot เวลาใหม่
  async CreateTimeslot({ request, response, auth }) {
    try {
      const account = await auth.getUser();
      if (account.role == "STAFF" || account.role == "ADMIN") {
        const { type_id, date, time_slot, doctor_id } = request.only([
          "type_id",
          "date",
          "time_slot",
          "doctor_id",
        ]);

        await validateAll(
          {
            type_id,
            date,
            time_slot,
          },
          CreateSlotRules
        );
        const dateNewFormat = new Date(date);
        const day_name = DateService.dateToDay(dateNewFormat.getDay());
        const work_times = await Database.table("work_times")
          .select("*")
          .where({
            type_id: type_id,
            day: day_name,
            doctor_id: doctor_id,
          })
          .first();

        //return 204 ในกรณี type_id และ date ไม่มีใน work_times
        if (work_times == undefined) {
          return response.status(204).send();
        }

        const con = `${work_times.working_id}${date
          .slice(0, 10)
          .replace(/-/g, "")}%`; //key = working_id + date + time_in
        const timeslot_db = await Database.from("bookings")
          .where("key_slot", "LIKE", con)
          .orderByRaw("time_in, date");

        const timeslot_old = timeslot_db.map(function (timeslot_db) {
          return timeslot_db.key_slot;
        });

        //วนลูป insert
        time_slot.forEach(async (element) => {
          let time_in = element;
          let time_out = new Date(
            new Date("1970/01/01 " + element).getTime() +
              work_times.time_slot * 60000
          ).toLocaleTimeString("en-UK", {
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit",
            hour12: false,
          });
          let add_id = account.account_id;
          let key_slot = `${work_times.working_id}${date
            .split("-")
            .join("")}${element.split(":").join("")}`;
          //console.log(key_slot);
          try {
            if (!timeslot_old.includes(key_slot)) {
              let b = await Booking.create({
                time_in: time_in,
                time_out: time_out,
                date: date,
                check_attention: "ABSENT",
                availability: "AVAILABLE",
                working_id: work_times.working_id,
                add_id: add_id,
                key_slot: key_slot,
              });
              //console.log(b);
            }
          } catch (error) {
            response.status(error.status).send();
          }
        });
        response.status(201).send();
      } else {
        return response.status(403).send("Forbidden");
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  async getDateService({ request, response, auth }) {
    try {
      const staff = await auth.getUser();
      if (staff.role == "STAFF" || staff.role == "ADMIN") {
        const { type_id, doctor_id } = request.only(["type_id", "doctor_id"]);
        const dateService = await Database.select(
          "bookings.working_id",
          "date",
          Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as dateformat')
        )
          .from("work_times")
          .innerJoin("bookings", "work_times.working_id", "bookings.working_id")
          .whereRaw(
            `type_id = ${type_id} AND doctor_id = ${doctor_id} AND bookings.date >= '${new Date()
              .toISOString()
              .slice(0, 10)}'`
          )
          .groupByRaw("working_id, date");
        let resData = dateService.map((date) => {
          let data = {
            working_id: date.working_id,
            date: moment(date.date).format("YYYY-MM-DD"),
            dateTH: DateService.ChangeDateFormat(date.dateformat),
          };
          return data;
        });
        return response.send(resData);
      } else {
        return response.status(403).send();
      }
    } catch (error) {
      console.log(error);
      return response.status(error.status).send(error);
    }
  }

  async checkEditTimeslot({ request, response, auth }) {
    try {
      const staff = await auth.getUser();
      const { type_id, date, doctor_id } = request.only([
        "type_id",
        "date",
        "doctor_id",
      ]);
      if (staff.role == "STAFF" || staff.role == "ADMIN") {
        const working_time = await Database.select("*")
          .from("work_times")
          .where({
            type_id: type_id,
            day: moment(date).format("dddd").toUpperCase(),
            doctor_id: doctor_id,
          })
          .first();
        if (working_time == undefined) {
          return response.status(204).send();
        }
        const bookings_slot = await Database.select(
          "booking_id",
          "time_in",
          "time_out",
          "date",
          "key_slot",
          "status",
          "availability",
          "working_id"
        )
          .from("bookings")
          .where({ working_id: working_time.working_id, date: date })
          .orderBy("time_in");

        let arr_booking = bookings_slot.map(function (bookings_slot) {
          if (bookings_slot.availability == "AVAILABLE") {
            Object.assign(bookings_slot, { toggle: true });
          } else if (bookings_slot.availability == "UNAVAILABLE") {
            Object.assign(bookings_slot, { toggle: false });
          }
          return bookings_slot;
        });

        if (bookings_slot.length > 0) {
          return response.json({
            type_id: type_id,
            date: date,
            date_use: DateService.ChangeDateFormat(
              moment(date).format("dddd D MMMM YYYY")
            ),
            timeArray: arr_booking,
          });
        } else {
          return response.send("Empty Slot");
        }
      } else {
        response.status(403).send();
      }
    } catch (error) {
      response.status(500).send(error);
    }
  }

  // edit only one slot availability
  async editOneSlot({ request, response, auth }) {
    try {
      const staff = await auth.getUser();
      const { booking_id, availability } = request.only([
        "booking_id",
        "availability",
      ]);
      if (staff.role == "STAFF" || staff.role == "ADMIN") {
        if (availability == "AVAILABLE" || availability == "UNAVAILABLE") {
          await Booking.query()
            .where("booking_id", booking_id)
            .update({
              availability:
                availability == "AVAILABLE" ? "AVAILABLE" : "UNAVAILABLE",
              edit_id: staff.account_id,
            });
          if ((await Booking.find(booking_id)).availability == availability) {
            return response.send("success");
          } else {
            return response.status(204).send();
          }
        } else {
          return response.send("can not update");
        }
      }
    } catch (error) {
      response.status(500).send(error);
    }
  }

  async editAllDaySlot({ request, response, auth }) {
    try {
      const staff = await auth.getUser();
      const { array_booking_id, availability } = request.only([
        "array_booking_id",
        "availability",
      ]);
      if (staff.role == "STAFF" || staff.role == "ADMIN") {
        if (availability == "AVAILABLE" || availability == "UNAVAILABLE") {
          await Booking.query()
            .whereIn("booking_id", array_booking_id)
            .update({
              availability:
                availability == "AVAILABLE" ? "AVAILABLE" : "UNAVAILABLE",
              edit_id: staff.account_id,
            });
          const booking = await Database.select("*")
            .from("bookings")
            .whereIn("booking_id", array_booking_id);
          let check_booking = booking.map(function (booking) {
            return booking.availability;
          });
          if (
            check_booking.includes(
              availability == "AVAILABLE" ? "UNAVAILABLE" : "AVAILABLE"
            ) == true
          ) {
            return response.send("can not update all");
          } else {
            return response.send("success");
          }
        } else {
          return response.send("can not update");
        }
      }
    } catch (error) {
      return response.status(500).send(error);
    }
  }

  // get servicetype สำหรับ staff แยกตาม service ที่ตัวเองให้บริการ
  async getServiceTypesStaff({ response, auth }) {
    try {
      const staff = await auth.getUser();
      let staff_serveice = null;
      if (staff.role == "STAFF") {
        staff_serveice = await Database.select(
          "servicetypes.type_id AS type_id",
          "work_times.doctor_id"
        )
          .select(
            Database.raw(
              `CONCAT (servicetypes.type_name,' (',location_name,') ') AS type_name`
            )
          )
          .from("servicetypes")
          .innerJoin("work_times", "servicetypes.type_id", "work_times.type_id")
          .innerJoin(
            "locations",
            "servicetypes.location_id",
            "locations.location_id"
          )
          .where({
            "work_times.doctor_id": staff.account_id,
            "work_times.availability": "AVAILABLE",
            "servicetypes.availability": "AVAILABLE",
          })
          .groupByRaw("servicetypes.type_id, work_times.doctor_id");
      } else if (staff.role == "ADMIN") {
        staff_serveice = await Database.select(
          "servicetypes.type_id",
          "work_times.doctor_id"
        )
          .select(
            Database.raw(
              `CONCAT (servicetypes.type_name,' (',location_name,') - ', (CASE WHEN accounts.prefix IS NOT NULL THEN accounts.prefix ELSE '' END) ,' ', accounts.first_name, ' ', accounts.last_name) AS type_name`
            )
          )
          .from("servicetypes")
          .innerJoin(
            "locations",
            "servicetypes.location_id",
            "locations.location_id"
          )
          .innerJoin("work_times", "work_times.type_id", "servicetypes.type_id")
          .innerJoin("accounts", "work_times.doctor_id", "accounts.account_id")
          .where({
            "servicetypes.availability": "AVAILABLE",
          })
          .groupByRaw("servicetypes.type_id, work_times.doctor_id");
      } else {
        return response.status(403).send();
      }
      return response.send(staff_serveice);
    } catch (error) {
      response.status(500).send(error);
    }
  }

  async getServiceDate({ request, response, auth }) {
    try {
      const { type_id, doctor_id } = request.only(["type_id", "doctor_id"]);
      const days = await Database.select("*").from("work_times").where({
        doctor_id: doctor_id,
        type_id: type_id,
        availability: "AVAILABLE",
      });
      //console.log(days);

      if (days.length > 0) {
        let data = [];
        days.forEach((day) => {
          let dayNumber;
          switch (day.day) {
            case "SUNDAY":
              dayNumber = 1;
              break;
            case "MONDAY":
              dayNumber = 2;
              break;
            case "TUESDAY":
              dayNumber = 3;
              break;
            case "WEDNESDAY":
              dayNumber = 4;
              break;
            case "THURSDAY":
              dayNumber = 5;
              break;
            case "FRIDAY":
              dayNumber = 6;
              break;
            case "SATURDAY":
              dayNumber = 7;
              break;
            default:
              dayNumber = null;
              break;
          }
          data.push(dayNumber);
        });
        return response.send(data);
      } else {
        return response.status(204).send();
      }
    } catch (error) {
      console.log(error);
      response.status(error.status).send(error);
    }
  }
}

module.exports = ManagebookingController;
