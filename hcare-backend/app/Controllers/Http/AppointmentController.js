"use strict";
const Booking = use("App/Models/Booking");
const Database = use("Database");
const Mail = use("Mail");
const Env = use("Env");
const DateFormat = use("App/Service/DateService");
const History = use("App/Models/History");

//Tesst
class AppointmentController {
  /*show appointment for patient*/
  async myAppointment({  response, auth }) {
    try {
      const account = await auth.getUser();
      if (account) {
        let dateNow = new Date().toISOString().slice(0, 10);
        let mybooking = await Database.select(
          "accounts.account_id as account_id",
          "hn_number",
          "first_name",
          "last_name",
          "booking_id",
          "work_times.type_id",
          "type_name",
          "date",
          "time_in",
          "link_meeting",
          "bookings.status",
          Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as dateformat')
        )
          .from("bookings")
          .innerJoin(
            "accounts",
            "bookings.account_id_from_user",
            "accounts.account_id"
          )
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .innerJoin(
            "servicetypes",
            "work_times.type_id",
            "servicetypes.type_id"
          )
          .where({
            account_id_from_user: account.account_id,
            status: "CONFIRM SUCCESS",
          })
          .whereRaw(`date >= "${dateNow}"`)
          .orderByRaw("bookings.date, bookings.time_in");

        for (let index = 0; index < mybooking.length; index++) {
          mybooking[index].dateformat = DateFormat.ChangeDateFormat(
            mybooking[index].dateformat
          );
        }

        if (!mybooking[0]) {
          response.status(204).send();
        }

        return mybooking;
      } else {
        response.status(401).send();
      }
    } catch (error) {
      response.status(500).send(error);
    }
  }

  //หน้า appointment detail เมื่อคลิกที่ card
  async myAppointmentDetail({  response, params, auth }) {
    try {
      const account = await auth.getUser();
      if (account) {
        //find doctor in account table
        const doctor = await Database.select(
          "accounts.prefix AS prefix",
          "accounts.account_id AS doctor_id",
          "first_name AS doctor_firstname",
          "last_name AS doctor_lastname"
        )
          .from("bookings")
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .innerJoin("accounts", "work_times.doctor_id", "accounts.account_id")
          .where({ booking_id: params.booking_id })
          .first();

        // find booking and personal data of patient
        const booking = await Database.select(
          "accounts.account_id as account_id",
          "hn_number",
          "first_name",
          "last_name",
          "booking_id",
          "work_times.type_id",
          "type_name",
          "date",
          "time_in",
          "link_meeting"
        )
          .select(
            Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as dateformat')
          )
          .from("bookings")
          .innerJoin(
            "accounts",
            "bookings.account_id_from_user",
            "accounts.account_id"
          )
          .innerJoin(
            "work_times",
            "bookings.working_id",
            "work_times.working_id"
          )
          .innerJoin(
            "servicetypes",
            "work_times.type_id",
            "servicetypes.type_id"
          )
          .where({
            booking_id: params.booking_id,
            status: "CONFIRM SUCCESS",
          })
          .first();

        booking.dateformat = DateFormat.ChangeDateFormat(booking.dateformat);

        const sendBooking = { ...booking, ...doctor };

        if (account.account_id == booking.account_id) {
          return sendBooking;
        } else {
          return response.status(403).send();
        }
      } else {
        return response.status(401).send();
      }

      //return sendBooking;
    } catch (error) {
      response.status(500).send(error);
    }
  }

  async cancelAppointmentFromAppointmentDetail({ request, response, auth }) {
    try {
      const { booking_id } = await request.only(["booking_id"]);

      if (booking_id) {
        const booking = await Booking.find(booking_id);
        const account = await auth.getUser();

        if (booking.status == "CONFIRM SUCCESS") {
          const findBooking = await Database.select(
            "bookings.booking_id AS booking_id",
            "servicetypes.type_name AS type_name",
            "bookings.time_in AS time_in",
            "bookings.time_out AS time_out",
            "bookings.date AS date",
            "bookings.status AS status"
          )
            .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as date'))
            .from("bookings")
            .innerJoin(
              "work_times",
              "bookings.working_id",
              "work_times.working_id"
            )
            .innerJoin(
              "servicetypes",
              "work_times.type_id",
              "servicetypes.type_id"
            )
            .where("bookings.booking_id", booking.booking_id)
            .first();

          findBooking.date = DateFormat.ChangeDateFormat(findBooking.date);
          findBooking.time_in = findBooking.time_in.substring(0, 5);
          findBooking.time_out = findBooking.time_out.substring(0, 5);

          const dataForSendEmail = {
            account: account,
            bookingSlot: findBooking,
            url: Env.get("VUE_APP_FONTEND_URL"),
            mail_subject: "คุณได้ยกเลิกการนัดหมายเรียบร้อยแล้ว",
          };

          const subject =
            "Cancel booking " +
            dataForSendEmail.bookingSlot.type_name.toString();

          await Mail.send("sendmailcancel", dataForSendEmail, (message) => {
            message
              .to(account.email)
              .from(Env.get("MAIL_USERNAME"))
              .subject(subject);
          });

          await Booking.query()
            .where({
              booking_id: booking_id,
              account_id_from_user: account.account_id,
            })
            .update({
              status: null,
              comment_from_user: null,
              comment_from_staff: null,
              token_booking_confirm: null,
              medical_note: null,
              link_meeting: null,
              account_id_from_user: null,
              account_id_from_staff: null,
            });

          await History.query()
            .where({ booking_id: booking_id, is_active: 1 })
            .update({ is_active: 0 });
          const bookingUpdate = await Booking.find(booking_id);
          return response.json({
            message: "clear schedule successful",
            booking: bookingUpdate,
          });
        } else {
          return response
            .status(304)
            .json({ message: "Don't have booking in database" });
        }
      } else {
        return response
          .status(500)
          .json({ message: "Booking ID does not exist" });
      }
    } catch (error) {
      console.log(error);
      response.status(500).send(error);
    }
  }
}

module.exports = AppointmentController;
