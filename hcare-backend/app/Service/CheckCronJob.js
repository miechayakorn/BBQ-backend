"use strict";
const moment = require("moment");
moment.locale();
const Database = use("Database");
const AutoCreateSlot = use("App/Service/AutoCreateSlot");

class CheckCronJob {
  static async checkCronJobCreateSlot() {
    try {
      const d = new Date().toISOString().slice(0, 10);
      const dDate = parseInt(d.substr(d.length - 2));
      console.log("Cronjob Check");
      if (dDate >= 10) {
        const monthYear = moment(new Date()).add(1, "M").format("YYYY-MM");
        const cm = await Database.select("*")
          .from("cronjoblogs")
          .whereRaw(`round = '${monthYear}' AND type = 'CREATE BOOKING SLOT'`);
        if (cm.length == 0) {
          const acs = await AutoCreateSlot.create();
        }
      }
      return true;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
module.exports = CheckCronJob;
