"use strict";

const Database = use("Database");
const Mail = use("Mail");
const Env = use("Env");
const DateFormat = use("App/Service/DateService");
const moment = require("moment");
moment.locale();

class SendMailBooking {
  static async sendMail() {
    try {
      const today = moment().format("YYYY-MM-DD");
      const list_booking = await Database.select("*")
        .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as dateformat'))
        .from("bookings")
        .innerJoin("work_times", "bookings.working_id", "work_times.working_id")
        .innerJoin("servicetypes", "work_times.type_id", "servicetypes.type_id")
        .innerJoin(
          "accounts",
          "bookings.account_id_from_user",
          "accounts.account_id"
        )
        .whereRaw(
          `bookings.date = '${today}' AND bookings.status = 'CONFIRM SUCCESS'`
        );

      list_booking.forEach((element) => {
        element.dateformat = DateFormat.ChangeDateFormat(element.dateformat);
        element.time_in = element.time_in.substring(0, 5);
        element.time_out = element.time_out.substring(0, 5);
      });

      if (list_booking.length > 0) {
        list_booking.forEach(async (element) => {
          let subject = "แจ้งเตือนการนัดหมาย " + element.type_name.toString();
          let mail = await Mail.send("sendmailnotice", element, (message) => {
            message
              .to(element.email)
              .from(Env.get("MAIL_USERNAME"))
              .subject(subject);
          });
          //console.log(mail);
        });
      }
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
module.exports = SendMailBooking;
