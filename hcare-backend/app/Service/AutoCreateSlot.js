"use strict";
const Database = use("Database");
const Booking = use("App/Models/Booking");
const Cronjoblog = use("App/Models/Cronjoblog");
const moment = require("moment");
moment.locale();

class AutoCreateSlot {
  static async create() {
    try {
      const serviceAvailable = await Database.select("*")
        .from("servicetypes")
        .innerJoin("work_times", "servicetypes.type_id", "work_times.type_id")
        .where({
          "servicetypes.availability": "AVAILABLE",
          "work_times.availability": "AVAILABLE",
        });
      if (serviceAvailable.length > 0) {
        console.log("Auto Create Slot");
        //หา account bot
        const bot = await Database.from("accounts")
          .where({ account_id: 1 })
          .first();

        //ค้นหา slot เดือนถัดไป
        const next_month = moment().add(1, "months").format("YYYY-MM");
        const oldslot = await Database.from("bookings").whereRaw(
          `DATE_FORMAT(date, "%Y-%m") = '${next_month}'`
        );

        //ค้นหา work_times
        const work_times = await Database.from("work_times").where({
          availability: "AVAILABLE",
        });

        //automate create slot
        const last_day = parseInt(
          moment().add(1, "months").endOf("month").format("D")
        );

        for (let day = 1; day <= last_day; day++) {
          let work_days = moment(`${next_month}-${day <= 9 ? "0" + day : day}`)
            .format("dddd")
            .toUpperCase();
          let work = work_times.filter((work_times) => {
            if (work_times.day == work_days) {
              return work_times;
            }
          });
          for (let work_slot = 0; work_slot < work.length; work_slot++) {
            let minsToAdd = work[work_slot].time_slot;
            let start_time = work[work_slot].start_time;
            let end_time = work[work_slot].end_time;
            let start = new Date(
              new Date(`1970/01/01"  ${start_time}`)
            ).getTime();
            let end = new Date(new Date(`1970/01/01"  ${end_time}`)).getTime();
            let round = (end - start) / 60000 / minsToAdd;
            let time_in = start_time;

            for (let i = 0; i < round; i++) {
              let time_out = new Date(
                new Date("1970/01/01 " + time_in).getTime() + minsToAdd * 60000
              ).toLocaleTimeString("en-UK", {
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit",
                hour12: false,
              });
              let key_slot = `${work[work_slot].working_id}${next_month
                .split("-")
                .join("")}${day <= 9 ? "0" + day : day}${time_in
                .split(":")
                .join("")}`;

              let findindex = oldslot.findIndex(
                (data) => data.key_slot == key_slot
              );

              if (findindex == -1) {
                await Booking.create({
                  time_in: time_in,
                  time_out: time_out,
                  date: `${next_month}-${day <= 9 ? "0" + day : day}`,
                  check_attention: "ABSENT",
                  availability: "AVAILABLE",
                  working_id: work[work_slot].working_id,
                  add_id: bot.account_id,
                  key_slot: key_slot,
                });
                time_in = time_out;
              }
            }
          }
        }
        await Cronjoblog.findOrCreate(
          {
            type: "CREATE BOOKING SLOT",
            round: next_month,
          },
          {
            type: "CREATE BOOKING SLOT",
            round: next_month,
          }
        );
        console.log("create booking slot successfully");
        return "create booking slot successfully";
      } else {
        console.log("No have any service available");
        return "No have any service available";
      }
    } catch (err) {
      console.log(err);
      return err;
    }
  }
}
module.exports = AutoCreateSlot;
