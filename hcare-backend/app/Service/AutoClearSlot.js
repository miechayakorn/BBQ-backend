"use strict";
const Database = use("Database");
const Booking = use("App/Models/Booking");
const moment = require("moment");
moment.locale();

class AutoCreateSlot {
  static async clear() {
    try {
      const previous_month = moment().subtract(1, "months").format("YYYY-MM");
      //console.log(previous_month);
      const clear_booking = await Booking.query()
        .whereRaw(
          `DATE_FORMAT(date, "%Y-%m") = '${previous_month}' AND status is NULL`
        )
        .delete();
      console.log("clear empty slot previous month success");
      return "clear empty slot previous month success";
    } catch (err) {
      return response.status(500).send(err);
    }
  }
}
module.exports = AutoCreateSlot;
