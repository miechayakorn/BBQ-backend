"use strict";
class CheckRole {
  //ใช้งานไม่ได้
  static checkAdmin = (role, response) => {
    if (role === "ADMIN") {
      return true;
    } else {
      return response.status(401).send();
    }
  };
}
module.exports = CheckRole;
