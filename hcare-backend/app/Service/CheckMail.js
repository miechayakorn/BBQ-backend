"use strict";
class CheckMail {
  static async checkMailKMUTT(mail) {
    const kmuttmail = ["@mail.kmutt.ac.th"];
    let mail1 = mail.substr(mail.length - 17);
    if (kmuttmail.includes(mail1)) {
      return true;
    } else {
      return false;
    }
  }
}
module.exports = CheckMail;
