"use strict";
const Database = use("Database");
const Mail = use("Mail");
const Env = use("Env");
const DateFormat = use("App/Service/DateService");
const moment = require("moment");
moment.locale();

class SendMailSurv {
  static async surv() {
    try {
      const hrAgo = moment().add(-1, "h").format("HH");
      const hrNow = moment().format("HH");

      const survList = await Database.select("*")
        .select(Database.raw('DATE_FORMAT(date, "%W %e %M %Y") as dateformat'))
        .from("bookings")
        .innerJoin("work_times", "bookings.working_id", "work_times.working_id")
        .innerJoin("servicetypes", "servicetypes.type_id", "work_times.type_id")
        .innerJoin(
          "accounts",
          "bookings.account_id_from_user",
          "accounts.account_id"
        )
        .whereRaw(
          `bookings.status = "CONFIRM SUCCESS" AND bookings.date = '${moment().format(
            "YYYY-MM-DD"
          )}' AND (bookings.time_out BETWEEN '${hrAgo}:01:00' AND '${hrNow}:00:00')`
        );
      console.log(survList);

      survList.forEach((element) => {
        element.dateformat = DateFormat.ChangeDateFormat(element.dateformat);
        element.time_in = element.time_in.substring(0, 5);
        element.time_out = element.time_out.substring(0, 5);
      });

      if (survList.length > 0) {
        survList.forEach(async (element) => {
          let subject = "แบบประเมิน " + element.type_name.toString();
          let mail = await Mail.send(
            "sendmailsurvroject",
            element,
            (message) => {
              message
                .to(element.email)
                .from(Env.get("MAIL_USERNAME"))
                .subject(subject);
            }
          );
          //console.log(mail);
        });
      } else {
        return "No have timeslot for send mail";
      }
    } catch (error) {
      console.log(error);
      return error.message;
    }
  }
}
module.exports = SendMailSurv;
