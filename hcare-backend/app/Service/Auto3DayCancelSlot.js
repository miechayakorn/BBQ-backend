"use strict";
const Database = use("Database");
const Booking = use("App/Models/Booking");
const moment = require("moment");
moment.locale();

class Auto3DayCancelSlot {
  static async clear() {
    try {
      const threedayago = moment().subtract(4, "days").format("YYYY-MM-DD");
      await Booking.query()
        .whereRaw(
          `status = "WAITING CONFIRM" AND SUBSTR(updated_at,1,10) = '${threedayago}' `
        )
        .update({
          status: null,
          comment_from_user: null,
          comment_from_staff: null,
          token_booking_confirm: null,
          link_meeting: null,
          account_id_from_user: null,
          account_id_from_staff: null,
        });
      console.log("clear slot success");
      return "clear slot success";
    } catch (err) {
      return response.status(500).send(err);
    }
  }
}
module.exports = Auto3DayCancelSlot;
