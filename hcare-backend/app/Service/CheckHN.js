"use strict";

const Account = use("App/Models/Account");

class CheckHN {
  static async checkHnInSystem(hn_number) {
    const checkHn = await Account.findBy("hn_number", hn_number);

    if (!checkHn) {
      return true;
    } else {
      return false;
    }
  }
}
module.exports = CheckHN;
