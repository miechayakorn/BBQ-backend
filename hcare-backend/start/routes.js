"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */

const Route = use("Route");
const Database = use("Database");
const Account = use("App/Models/Account");
const schedule = require("node-schedule");
const AutoCreateSlot = use("App/Service/AutoCreateSlot");
const AutoClearSlot = use("App/Service/AutoClearSlot");
const Auto3DayCancelSlot = use("App/Service/Auto3DayCancelSlot");
const SendMailBooking = use("App/Service/SendMailBooking");
const SendMailSurv = use("App/Service/SendMailSurv");
const CheckCronJob = use("App/Service/CheckCronJob");

Route.get("/allaccount", async ({ request, response }) => {
  const allAcc = await Account.all();
  return response.send(allAcc);
});

//============ Corn Job ======================================================================
schedule.scheduleJob("0 23 15 * *", async function () {
  try {
    const createslot = AutoCreateSlot.create();
  } catch (error) {
    console.log(error);
  }
});
schedule.scheduleJob("@monthly", function () {
  //const clearslot = AutoClearSlot.clear();
});
schedule.scheduleJob("@daily", function () {
  try {
    const clear = Auto3DayCancelSlot.clear();
  } catch (error) {
    console.log(error);
  }
});
schedule.scheduleJob("@daily", async function () {
  try {
    const mail = await SendMailBooking.sendMail();
  } catch (error) {
    console.log(error);
  }
});
schedule.scheduleJob("@hourly", async function () {
  try {
    const survMail = await SendMailSurv.surv();
    const ccj = await CheckCronJob.checkCronJobCreateSlot();
  } catch (error) {
    console.log(error);
  }
});



//==========================================================================================

Route.on("/").render("welcome");

Route.get("/checktoken", "CheckTokenController.check").middleware("auth");

//============================ user register & login =========================================
Route.post("/register", "UserRegisterController.createUser");
Route.get("/register/confirm", "UserRegisterController.confirmRegister");
Route.group(() => {
  Route.post("/login", "AuthController.authenticate");
  Route.post("/login/confirm", "AuthController.confirmauthenticate");
  Route.post("/login/oauth", "AuthController.oauth");
}).middleware("guest");
Route.get("/user/me", "AuthController.myprofile").middleware("auth"); //test token
Route.get("/logout", "AuthController.logout").middleware("auth");

//=============== staff and admin authentication (Register & Login & Forget password) ==========================
//Route.post("/staff/register", "HealthcareStaffAuthController.createStaff");
Route.group(() => {
  Route.get(
    "/staff/register/confirm",
    "HealthcareStaffAuthController.confirmRegister"
  );
  Route.post("staff/login", "HealthcareStaffAuthController.staffLogin");
}).middleware(["guest"]);
Route.post(
  "admin/forgetpassword",
  "HealthcareStaffAuthController.staffForgetPassword"
);
Route.post(
  "admin/forgetpassword/confirm",
  "HealthcareStaffAuthController.staffConfirmForgetPassword"
);
//============================================================================================

//================================== for booking feature =====================================
Route.get("/ServiceTypes/:location_id", "BookingController.showType");
Route.get("/ServiceDate/:type_id", "BookingController.showDate");
Route.get("/ServiceTime", "BookingController.showTime");
Route.get("/servicedoctor", "BookingController.showDoctor");
Route.group(() => {
  Route.post("/Booking", "BookingController.submitBooking");
}).middleware("auth");
Route.get("/bookings/confirm", "BookingController.confirmBooking");
Route.get("/location", "BookingController.getLocation");
Route.get("/announcement", "ServiceController.announcementAllService");
Route.get("search/email", "BookingController.searchEmail");
//=============================================================================================

//================================= service schedule ==========================================
Route.post(
  "/serviceschedule/getservice",
  "ServiceScheduleController.getServiceSchedule"
);
Route.post(
  "/serviceschedule/getservicedetail",
  "ServiceScheduleController.getServiceScheduleDetail"
);
//=============================================================================================

//============================ show booking for individual user ==============================
Route.group(() => {
  Route.get("/myappointment", "AppointmentController.myAppointment");
  Route.get(
    "/appointment/detail/:booking_id",
    "AppointmentController.myAppointmentDetail"
  );
  Route.post(
    "/appointment/cancel",
    "AppointmentController.cancelAppointmentFromAppointmentDetail"
  );
}).middleware("auth");
//============================================================================================

//============================== Dashboard for staff and admin ===============================
Route.group(() => {
  Route.get(
    "/showbooking/:type/:date",
    "DashboardBookingController.showBookingForHCARE"
  );
  Route.post(
    "/patientbooking/edit",
    "DashboardBookingController.editPatientBooking"
  );
  Route.post(
    "/booking/healthcare",
    "DashboardBookingController.submitBookingFromHealthcare"
  );
  Route.post(
    "/admin/dashboard/timetable/managetable/checktimeslot",
    "ManagebookingController.CheckTimeslot"
  );
  Route.post(
    "/admin/dashboard/timetable/managetable/checkdate",
    "ManagebookingController.getServiceDate"
  );
  Route.post(
    "/admin/dashboard/timetable/managetable/savetimeslot",
    "ManagebookingController.CreateTimeslot"
  );
  Route.get(
    "/ServiceTypesStaff",
    "ManagebookingController.getServiceTypesStaff"
  );
  Route.post(
    "/admin/dashboard/timetable/EditSlotTime/checkslot",
    "ManagebookingController.checkEditTimeslot"
  );
  Route.post(
    "/admin/dashboard/timetable/EditSlotTime/checkdate",
    "ManagebookingController.getDateService"
  );
  Route.patch(
    "/admin/dashboard/timetable/EditSlotTime/oneslotsave",
    "ManagebookingController.editOneSlot"
  );
  Route.put(
    "/admin/dashboard/timetable/EditSlotTime/allslotsave",
    "ManagebookingController.editAllDaySlot"
  );
  Route.post(
    "/admin/dashboard/manageemployee/create",
    "ManageEmployeeController.createEmployee"
  );
  Route.get(
    "/admin/dashboard/manageemployee/getemployee",
    "ManageEmployeeController.getEmployees"
  );
  Route.get(
    "/admin/dashboard/manageemployee/getemployeeforedit",
    "ManageEmployeeController.getOnlyoneEmployeeData"
  );
  Route.patch(
    "/admin/dashboard/manageemployee/editemployee",
    "ManageEmployeeController.editEmployeeSave"
  );
  Route.get("/admin/history", "HistoryController.getHistoryForStaff");
  Route.get("/history", "HistoryController.getHistoryForUser");
  Route.get(
    "/history/detail/:booking_id",
    "HistoryController.getHistoryDetail"
  );
  Route.get("/service", "ServiceController.getService");
  Route.get("/service/worktimeperson", "WorkTimeController.getWorkTimePerson");
  Route.get("/getlocations", "LocationController.getLocations");
  Route.post("/addlocation", "LocationController.createLocation");
  Route.patch(
    "/updatelocationstatus",
    "LocationController.updateLocationStatus"
  );
  Route.post("/addservice", "ServiceController.createService");
  Route.get("/getservice", "ServiceController.getServiceForAdd");
  Route.get(
    "/getserviceresposibleperson",
    "WorkTimeController.getServiceResponsiblePerson"
  );
  Route.get("/getemployee", "ServiceController.getEmployeesForAdd");
  Route.post("/addworktime", "WorkTimeController.addWorkTime");
  Route.get("/getworktimes", "WorkTimeController.getWorkTimes");
  //Route.get("/editservice/getservice", "ServiceController.getServiceForEdit");
  // Route.get(
  //   "/editservice/admingetemployee",
  //   "ServiceController.getEmployeesForEditService"
  // );
  //Route.post("/editservice/getworktimes", "WorkTimeController.getDayOfWorks");
  Route.get(
    "/editservice/getworktimes/detail",
    "WorkTimeController.getWorkTimeDetail"
  );
  Route.patch("/editservice/update", "WorkTimeController.updateWorktime");
  Route.patch(
    "/editservice/updatestatus",
    "WorkTimeController.updateWorktimeStatus"
  );
  Route.patch("/service/updatestatus", "ServiceController.updateServiceStatus");
  Route.get(
    "/delete/location/:location_id",
    "LocationController.deleteLocation"
  );
  Route.get("/delete/service/:type_id", "ServiceController.deleteService");
  Route.get(
    "/delete/worktimes/:working_id",
    "WorkTimeController.deleteWorktimes"
  );
  Route.post(
    "/admin/deleteemployee",
    "ManageEmployeeController.DeleteEmployee"
  );
}).middleware(["auth"]);

Route.get(
  "/admin/register/getnewemployee",
  "ManageEmployeeController.getNewEmployee"
);
Route.post(
  "/admin/register/savenewemployee",
  "ManageEmployeeController.savePersonalDataEmployee"
);

//============================================================================================

//======================================= Staff and User get profile =============================
Route.post(
  "/upload/profilepicture",
  "AccountController.uploadProfilePicture"
).middleware(["auth"]);
Route.get(
  "/editprofile/getprofile",
  "AccountController.getUserProfile"
).middleware(["auth"]);
Route.post(
  "/editprofile/save",
  "AccountController.saveEditProfile"
).middleware(["auth"]);
//============================================================================================

//======================================= Cancel Booking =====================================
Route.post("/cancel", "DashboardBookingController.cancelAppointment");
//============================================================================================
