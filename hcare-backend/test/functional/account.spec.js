"use strict";

const { test, trait } = use("Test/Suite")("Account");
const Account = use("App/Models/Account");
const Env = use("Env");

trait("Test/ApiClient");

test("check system detail", async ({ assert, client }) => {
  const account = await Account.findBy("email", "admin@mail.kmutt.ac.th");
  if (!account) {
    Account.create({
      account_id: 1,
      hn_number: 1,
      password: Env.get("DB_PASSWORD"),
      first_name: "SYSTEM",
      last_name: "SERVICE",
      verify: "SUCCESS",
      email: "admin@mail.kmutt.ac.th",
      role: "ADMIN",
    });
  }

  const response = await client.get("/allaccount").end();

  response.assertStatus(200);
  response.assertJSONSubset([
    {
      account_id: 1,
      first_name: "SYSTEM",
      last_name: "SERVICE",
      email: "admin@mail.kmutt.ac.th",
      role: "ADMIN",
    },
  ]);
});

test("check system account", async ({ assert }) => {
  const account = await Account.find(1);
  assert.equal(account.toJSON().account_id, 1);
});

test("check admin account", async ({ assert }) => {
  const account = await Account.query().where("role", "ADMIN").getCount();
  assert.isAbove(account, 0);
});
